//
// Created by gdhsnlvr on 03.06.17.
//

#ifndef GEOMETRY_LIBRARY_API_POLYGON_VECTOR_H
#define GEOMETRY_LIBRARY_API_POLYGON_VECTOR_H

#include <vector>
#include "../geometry/polygon.h"

class api_polygon_vector {
protected:
    std::vector<geometry::polygon*> _vector;

public:
    api_polygon_vector(const std::vector<geometry::polygon*> &v);
    api_polygon_vector(const std::vector<geometry::polygon> &v);
    api_polygon_vector(geometry::polygon **array, int n);

    geometry::polygon* get(int i) const;
    void set(int i, geometry::polygon *value);
    int size() const;

    std::vector<geometry::polygon> polygons() const;
};


#endif //GEOMETRY_LIBRARY_API_POLYGON_VECTOR_H
