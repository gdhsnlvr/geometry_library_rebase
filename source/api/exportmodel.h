//
// Created by User on 13.10.2016.
//

#ifndef GEOMETRY_LIBRARY_EXPORTMODEL_H
#define GEOMETRY_LIBRARY_EXPORTMODEL_H

#include "api.h"

API_DLL int horizon_count();
API_DLL int horizon_model_id(int index);
API_DLL int surface_model_id();

API_DLL int buffer_length(int id);
API_DLL double *position_buffer(int id);
API_DLL double *normal_buffer(int id);
API_DLL double *color_buffer(int id);

API_DLL int patch_buffer_length(int id);
API_DLL double *patch_position_buffer(int id);
API_DLL double *patch_normal_buffer(int id);
API_DLL double *patch_color_buffer(int id);

#endif //GEOMETRY_LIBRARY_EXPORTMODEL_H
