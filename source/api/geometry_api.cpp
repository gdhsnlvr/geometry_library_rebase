//
// Created by gdhsnlvr on 02.06.17.
//

#include "geometry_api.h"
#include "../geometry/detail/geometry_detail.h"
#include "api_storage.h"
#include "../geometry/detail/mesh_cross.h"
#include "../geometry/vec2.h"
#include "../geometry/detail/polygon_detail.h"

geometry::mesh *api_build_surface(double *x, double *y, double *z, int count) {
    std::vector<geometry::vec3> initial;
    for (int i = 0; i < count; i++) {
        initial.emplace_back(x[i], y[i], z[i]);
    }

    geometry::mesh *surface = new geometry::mesh();
    *surface = geometry::detail::buildMesh(initial);
    return surface;
}

geometry::mesh *api_build_horizon(geometry::mesh *mesh, double from, double to) {
    geometry::mesh *surface = new geometry::mesh();
    *surface = geometry::detail::slice_mesh(*mesh, from, to);
    return surface;
}

geometry::mesh *api_build_cross(geometry::mesh *mesh, double *x, double *y, int n) {
    std::vector<geometry::vec2> points;
    for (int i = 0; i < n; i++) {
        points.emplace_back(x[i], y[i]);
    }

    auto result = geometry::get_heights_on_cross(*mesh, points);

    std::vector<geometry::triangle> triangles;
    for (int i = 1; i < result.size(); i++) {
        geometry::triangle t1 = {
                {result[i-1].x, result[i-1].y, 1},
                {result[i-1].x, 0, 1},
                {result[i].x, 0, 1}
        };
        geometry::triangle t2 = {
                {result[i].x, 0, 1},
                {result[i].x, result[i].y, 1},
                {result[i-1].x, result[i-1].y, 1}
        };
        triangles.push_back(t1);
        triangles.push_back(t2);
    }

    return new geometry::mesh(triangles);
}

geometry::mesh *api_cut_mesh(geometry::mesh *mesh, geometry::polygon *polygon) {
    return new geometry::mesh(geometry::detail::cut_mesh(*mesh, *polygon));
}

double api_ray_pick_parameter(geometry::mesh *mesh,
                              double px, double py, double pz,
                              double ex, double ey, double ez) {
    geometry::vec3 position = {px, py, pz};
    geometry::vec3 eyeDir = {ex, ey, ez};
    geometry::line<geometry::vec3> ray = {position, eyeDir};
    return geometry::detail::rayCasting(*mesh, ray);
}

api_double_vector *api_position_buffer(geometry::mesh *mesh) {
    std::vector<double> buffer;
    for (auto triangle : mesh->triangles()) {
        for (int i = 0; i < 3; i++) {
            buffer.push_back(triangle[i].position.x);
            buffer.push_back(triangle[i].position.y);
            buffer.push_back(triangle[i].position.z);
        }
    }
    return new api_double_vector(buffer);
}

api_double_vector *api_normal_buffer(geometry::mesh *mesh) {
    std::vector<double> buffer;
    for (auto triangle : mesh->triangles()) {
        for (int i = 0; i < 3; i++) {
            buffer.push_back(triangle[i].normal.x);
            buffer.push_back(triangle[i].normal.y);
            buffer.push_back(triangle[i].normal.z);
        }
    }
    return new api_double_vector(buffer);
}

api_double_vector *api_color_buffer(geometry::mesh *mesh) {
    std::vector<double> buffer;
    for (auto triangle : mesh->triangles()) {
        for (int i = 0; i < 3; i++) {
            buffer.push_back(triangle[i].color.x);
            buffer.push_back(triangle[i].color.y);
            buffer.push_back(triangle[i].color.z);
        }
    }
    return new api_double_vector(buffer);
}

// vec2
geometry::vec2 *api_vec2_new(double x, double y) {
    return new geometry::vec2(x, y);
}

double api_vec2_x(geometry::vec2 *obj) {
    return obj->x;
}

double api_vec2_y(geometry::vec2 *obj) {
    return obj->y;
}

void api_vec2_free(geometry::vec2 *obj) {
    delete obj;
}

// polygon
geometry::polygon *api_polygon_new(api_vec2_vector *contour) {
    return new geometry::polygon(contour->points());
}

api_vec2_vector *api_polygon_outer(geometry::polygon *polygon) {
    return new api_vec2_vector(polygon->outer());
}

api_vec2_vector *api_polygon_inner(geometry::polygon *polygon, int index) {
    return new api_vec2_vector(polygon->inners()[index]);
}

int api_polygon_inner_count(geometry::polygon *polygon) {
    return (int) polygon->inners().size();
}

void api_polygon_free(geometry::polygon *polygon) {
    delete polygon;
}

api_polygon_vector *api_polygon_unite(geometry::polygon *a, geometry::polygon *b) {
    return new api_polygon_vector(geometry::unite(*a, *b));
}

api_polygon_vector *api_polygon_intersect(geometry::polygon *a, geometry::polygon *b) {
    return new api_polygon_vector(geometry::intersect(*a, *b));
}

api_polygon_vector *api_polygon_difference(geometry::polygon *a, geometry::polygon *b) {
    return new api_polygon_vector(geometry::difference(*a, *b));
}

geometry::mesh *api_polygon_build_mesh(geometry::polygon *polygon) {
    return new geometry::mesh(geometry::build_mesh(*polygon));
}
