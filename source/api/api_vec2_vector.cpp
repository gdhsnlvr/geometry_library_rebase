//
// Created by gdhsnlvr on 03.06.17.
//

#include "api_vec2_vector.h"
#include "api.h"

api_vec2_vector::api_vec2_vector(const std::vector<geometry::vec2*> &v)
    : _vector(v)
{
}

api_vec2_vector::api_vec2_vector(const std::vector<geometry::vec2> &v) {
    for (auto p : v) {
        _vector.push_back(new geometry::vec2(p));
    }
}

api_vec2_vector::api_vec2_vector(geometry::vec2 **array, int n)
    : _vector(array, array + n)
{
}

geometry::vec2* api_vec2_vector::get(int i) const {
    return _vector[i];
}

void api_vec2_vector::set(int i, geometry::vec2* value) {
    _vector[i] = value;
}

int api_vec2_vector::size() const {
    return (int) _vector.size();
}

std::vector<geometry::vec2> api_vec2_vector::points() const {
    std::vector<geometry::vec2> result;
    for (auto v : _vector) {
        result.push_back(*v);
    }
    return result;
}

API_DLL api_vec2_vector * api_vec2_vector_new(geometry::vec2 **array, int n) {
    return new api_vec2_vector(array, n);
}

API_DLL geometry::vec2* api_double_vector_get(api_vec2_vector * obj, int i) {
    return obj->get(i);
}

API_DLL void api_double_vector_set(api_vec2_vector * obj, int i, geometry::vec2* value) {
    obj->set(i, value);
}

API_DLL int api_double_vector_size(api_vec2_vector * obj) {
    return obj->size();
}

API_DLL void api_double_vector_free(api_vec2_vector * obj) {
    delete obj;
}