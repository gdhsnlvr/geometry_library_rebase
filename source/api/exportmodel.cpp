//
// Created by User on 13.10.2016.
//

#include "exportmodel.h"
#include "api_storage.h"
#include "../geometry/detail/patch_building.h"

int horizon_count()
{
    Deposit *deposit = api_storage::instance().deposit();
    return (int) deposit->horizons().size();
}

int horizon_model_id(int index) {
    return index;
}

int surface_model_id() {
    return (int) api_storage::instance().deposit()->horizons().size();
}

int buffer_length(int id)
{
    const geometry::mesh &mesh = api_storage::instance().get_model(id);
    return (int) mesh.triangles().size() * 3 * 3;
}

double *position_buffer(int id)
{
    const geometry::mesh &mesh = api_storage::instance().get_model(id);

    double *buffer = new double[buffer_length(id)];
    int offset = 0;
    for (const geometry::triangle &triangle : mesh.triangles()) {
        for (int i = 0; i < 3; i++) {
            buffer[offset++] = triangle[i].position.x;
            buffer[offset++] = triangle[i].position.y;
            buffer[offset++] = triangle[i].position.z;
        }
    }

    return buffer;
}

double *normal_buffer(int id)
{
    const geometry::mesh &mesh = api_storage::instance().get_model(id);

    double *buffer = new double[buffer_length(id)];
    int offset = 0;
    for (const geometry::triangle &triangle : mesh.triangles()) {
        for (int i = 0; i < 3; i++) {
            buffer[offset++] = triangle[i].normal.x;
            buffer[offset++] = triangle[i].normal.y;
            buffer[offset++] = triangle[i].normal.z;
        }
    }

    return buffer;
}

double *color_buffer(int id)
{
    const geometry::mesh &mesh = api_storage::instance().get_model(id);

    double *buffer = new double[buffer_length(id)];
    int offset = 0;
    for (const geometry::triangle &triangle : mesh.triangles()) {
        for (int i = 0; i < 3; i++) {
            buffer[offset++] = triangle[i].color.x;
            buffer[offset++] = triangle[i].color.y;
            buffer[offset++] = triangle[i].color.z;
        }
    }

    return buffer;
}

int patch_buffer_length(int id)
{
    const geometry::quad_mesh &mesh = api_storage::instance().get_model_patches(id);
    return (int) mesh.quads().size() * 4 * 3;
}

double *patch_position_buffer(int id)
{
    const geometry::quad_mesh &mesh = api_storage::instance().get_model_patches(id);

    double *buffer = new double[patch_buffer_length(id)];
    int offset = 0;
    for (const geometry::quad &quad : mesh.quads()) {
        for (int i = 0; i < 4; i++) {
            buffer[offset++] = quad[i].position.x;
            buffer[offset++] = quad[i].position.y;
            buffer[offset++] = quad[i].position.z;
        }
    }

    return buffer;
}

double *patch_normal_buffer(int id)
{
    const geometry::quad_mesh &mesh = api_storage::instance().get_model_patches(id);

    double *buffer = new double[patch_buffer_length(id)];
    int offset = 0;
    for (const geometry::quad &quad : mesh.quads()) {
        for (int i = 0; i < 4; i++) {
            buffer[offset++] = quad[i].normal.x;
            buffer[offset++] = quad[i].normal.y;
            buffer[offset++] = quad[i].normal.z;
        }
    }

    return buffer;
}

double *patch_color_buffer(int id)
{
    const geometry::quad_mesh &mesh = api_storage::instance().get_model_patches(id);

    double *buffer = new double[patch_buffer_length(id)];
    int offset = 0;
    for (const geometry::quad &quad : mesh.quads()) {
        for (int i = 0; i < 4; i++) {
            buffer[offset++] = quad[i].color.x;
            buffer[offset++] = quad[i].color.y;
            buffer[offset++] = quad[i].color.z;
        }
    }

    return buffer;
}
