//
// Created by gdhsnlvr on 03.06.17.
//

#include "api_polygon_vector.h"
#include "api.h"

api_polygon_vector::api_polygon_vector(const std::vector<geometry::polygon *> &v)
    : _vector(v)
{

}

api_polygon_vector::api_polygon_vector(const std::vector<geometry::polygon> &v) {
    for (auto p : v) {
        _vector.push_back(new geometry::polygon(p));
    }
}

api_polygon_vector::api_polygon_vector(geometry::polygon **array, int n)
    : _vector(array, array + n)
{

}

geometry::polygon *api_polygon_vector::get(int i) const {
    return _vector[i];
}

void api_polygon_vector::set(int i, geometry::polygon *value) {
    _vector[i] = value;
}

int api_polygon_vector::size() const {
    return (int) _vector.size();
}

std::vector<geometry::polygon> api_polygon_vector::polygons() const {
    return std::vector<geometry::polygon>();
}

API_DLL api_polygon_vector * api_polygon_vector_new(geometry::polygon **array, int n) {
    return new api_polygon_vector(array, n);
}

API_DLL geometry::polygon* api_polygon_vector_get(api_polygon_vector * obj, int i) {
    return obj->get(i);
}

API_DLL void api_polygon_vector_set(api_polygon_vector * obj, int i, geometry::polygon* value) {
    obj->set(i, value);
}

API_DLL int api_polygon_vector_size(api_polygon_vector * obj) {
    return obj->size();
}

API_DLL void api_polygon_vector_free(api_polygon_vector * obj) {
    delete obj;
}