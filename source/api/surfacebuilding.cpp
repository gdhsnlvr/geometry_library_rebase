//
// Created by User on 13.10.2016.
//

#include "surfacebuilding.h"
#include "api_storage.h"

#include "../geometry/vec3.h"
#include "../geometry/detail/geometry_detail.h"
#include "../geometry/io/geometry_io.h"
#include "../deposit/io/deposit_io.h"
#include "../geometry/detail/patch_building.h"

#include <iostream>
#include <vector>

void build_surface(double *x, double *y, double *z, int count) {
    std::vector<geometry::vec3> initial;
    for (int i = 0; i < count; i++) {
        initial.emplace_back(x[i], y[i], z[i]);
    }

    geometry::mesh surface = geometry::detail::buildMesh(initial);

    api_storage::instance().deposit()->setSurface(
            surface
    );
}

void build_horizons(double *h_begin, double *h_end, int count) {
    std::vector<real_t> heights_begin, heights_end;
    for (int i = 0; i < count; i++) {
        heights_begin.push_back(h_begin[i]);
        heights_end.push_back(h_end[i]);
    }

    api_storage::instance().deposit()->buildHorizons(
            heights_begin, heights_end
    );
}

void build_model_patches(int id, int patch_size, int patch_count)
{
    const geometry::mesh &mesh = api_storage::instance().get_model(id);

    geometry::vec3 size = (mesh.maxBoundary() - mesh.minBoundary()) / patch_count;
    geometry::quad_mesh patched_mesh = geometry::build_patched_mesh(mesh, size, patch_size - 1);
    api_storage::instance().set_model_patches(id, patched_mesh);
}

void load_surface(const char *filePath) {
    geometry::mesh mesh = geometry::io::read<geometry::mesh>(
            std::string(filePath)
    );

    std::cout << mesh.triangles().size() << std::endl;

    api_storage::instance().deposit()->setSurface(mesh);
}

void load_horizons(const char *filePath) {
    // TODO: load
}

void save_surface(const char *filePath) {
    Deposit *deposit = api_storage::instance().deposit();
    geometry::io::write(deposit->surface(), std::string(filePath));
}

void save_horizons(const char *filePath) {
    Deposit *deposit = api_storage::instance().deposit();
    for (Horizon *horizon : deposit->horizons()) {
        deposit::io::write(horizon, std::string(filePath));
    }
}
