//
// Created by gdhsnlvr on 20.01.17.
//

#ifndef GEOMETRY_LIBRARY_API_STORAGE_H
#define GEOMETRY_LIBRARY_API_STORAGE_H

#include "api.h"
#include "../deposit/Deposit.h"
#include "../geometry/quad_mesh.h"

class api_storage
{
protected:
    Deposit *m_deposit;

public:
    api_storage();
    ~api_storage();

    Deposit *deposit();

    const geometry::mesh &get_model(int index) const;
    const geometry::quad_mesh &get_model_patches(int index) const;
    void set_model_patches(int id, const geometry::quad_mesh &patched_mesh);
    int get_model_count() const;

    static api_storage &instance();
};


#endif //GEOMETRY_LIBRARY_API_STORAGE_H
