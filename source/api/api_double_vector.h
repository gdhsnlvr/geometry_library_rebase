//
// Created by gdhsnlvr on 27.05.17.
//

#ifndef GEOMETRY_LIBRARY_API_VECTOR_H
#define GEOMETRY_LIBRARY_API_VECTOR_H

#include <vector>

class api_double_vector {
protected:
    std::vector<double> _vector;

public:
    api_double_vector(const std::vector<double> &v);
    api_double_vector(double *array, int n);

    double get(int i) const;
    void set(int i, double value);
    int size() const;
};


#endif //GEOMETRY_LIBRARY_API_VECTOR_H
