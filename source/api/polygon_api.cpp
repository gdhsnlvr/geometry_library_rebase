//
// Created by gdhsnlvr on 11.03.17.
//

#include "polygon_api.h"
#include "../geometry/detail/polygon_detail.h"

static_polygon_buffer &static_polygon_buffer::get() {
    static static_polygon_buffer polygon_buffer;
    return polygon_buffer;
}

void static_polygon_buffer::set_clip_polygon(const geometry::polygon &polygon) {
    m_clip_polygon = polygon;
}

void static_polygon_buffer::set_object_polygon(const geometry::polygon &polygon) {
    m_object_polygon = polygon;
}

const geometry::polygon &static_polygon_buffer::clip_polygon() const {
    return m_clip_polygon;
}

const geometry::polygon &static_polygon_buffer::object_polygon() const {
    return m_object_polygon;
}

const std::vector<geometry::polygon> &static_polygon_buffer::result_buffer() const {
    return m_result_buffer;
}

std::vector<geometry::polygon> &static_polygon_buffer::r_result_buffer() {
    return m_result_buffer;
}

geometry::polygon get_polygon_from_buffer(int contour_count, int *len, double *position_buffer) {
    geometry::polygon polygon;
    for (int i = 0, index = 0; i < contour_count; i++) {
        geometry::polygon::contour_t contour;
        for (int j = 0; j < len[i]; j++) {
            double x = position_buffer[index++];
            double y = position_buffer[index++];
            contour.emplace_back(x, y);
        }
        if (i == 0) polygon.set_outer(contour);
        else polygon.add_inner(contour);
    }
    return polygon;
}

void set_clip_polygon(int contour_count, int *len, double *position_buffer) {
    static_polygon_buffer::get().set_clip_polygon(
            get_polygon_from_buffer(contour_count, len, position_buffer)
    );
}

void set_object_polygon(int contour_count, int *len, double *position_buffer) {
    static_polygon_buffer::get().set_object_polygon(
            get_polygon_from_buffer(contour_count, len, position_buffer)
    );
}

void push_find_difference() {
    const auto &clip_polygon = static_polygon_buffer::get().clip_polygon();
    const auto &object_polygon = static_polygon_buffer::get().object_polygon();
    auto result = geometry::difference(clip_polygon, object_polygon);
    auto &buffer = static_polygon_buffer::get().r_result_buffer();
    buffer.insert(buffer.end(), result.begin(), result.end());
}

void push_find_intersection() {
    const auto &clip_polygon = static_polygon_buffer::get().clip_polygon();
    const auto &object_polygon = static_polygon_buffer::get().object_polygon();
    auto result = geometry::intersect(clip_polygon, object_polygon);
    auto &buffer = static_polygon_buffer::get().r_result_buffer();
    buffer.insert(buffer.end(), result.begin(), result.end());
}

void push_find_union() {
    const auto &clip_polygon = static_polygon_buffer::get().clip_polygon();
    const auto &object_polygon = static_polygon_buffer::get().object_polygon();
    auto result = geometry::unite(clip_polygon, object_polygon);
    auto &buffer = static_polygon_buffer::get().r_result_buffer();
    buffer.insert(buffer.end(), result.begin(), result.end());
}

int peek_result_size() {
    return (int) static_polygon_buffer::get().result_buffer().size();
}

int peek_result_polygon_buffer_size() {
    geometry::polygon polygon = static_polygon_buffer::get().result_buffer().back();

    int sum_len = (int) polygon.outer().size();
    for (int i = 0; i < (int) polygon.inners().size(); i++) {
        sum_len += polygon.inners().at(i).size();
    }
    return sum_len;
}

int peek_result_polygon_contour_count() {
    return (int) static_polygon_buffer::get().result_buffer().back().inners().size() + 1;
}

void pop_result_polygon(int *len, double *position_buffer) {
    geometry::polygon polygon = static_polygon_buffer::get().result_buffer().back();

    int index = 0;
    len[0] = (int) polygon.outer().size();
    for (const auto &pt : polygon.outer()) {
        position_buffer[index++] = pt.x;
        position_buffer[index++] = pt.y;
    }

    for (int i = 0; i < (int) polygon.inners().size(); i++) {
        len[i + 1] = (int) polygon.inners().at(i).size();
        for (const auto &pt : polygon.inners().at(i)) {
            position_buffer[index++] = pt.x;
            position_buffer[index++] = pt.y;
        }
    }

    static_polygon_buffer::get().r_result_buffer().pop_back();
}
