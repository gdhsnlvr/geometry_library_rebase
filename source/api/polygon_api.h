//
// Created by gdhsnlvr on 11.03.17.
//

#ifndef GEOMETRY_LIBRARY_REGION_API_H
#define GEOMETRY_LIBRARY_REGION_API_H

#include "api.h"
#include "../geometry/polygon.h"

class static_polygon_buffer {
protected:
    geometry::polygon m_clip_polygon;
    geometry::polygon m_object_polygon;
    std::vector<geometry::polygon> m_result_buffer;

public:
    static static_polygon_buffer &get();

    void set_clip_polygon(const geometry::polygon &polygon);
    void set_object_polygon(const geometry::polygon &polygon);

    const geometry::polygon &clip_polygon() const;
    const geometry::polygon &object_polygon() const;
    const std::vector<geometry::polygon> &result_buffer() const;

    std::vector<geometry::polygon> &r_result_buffer();
};

geometry::polygon get_polygon_from_buffer(int contour_count, int *len, double *position_buffer);
API_DLL void set_clip_polygon(int contour_count, int *len, double *position_buffer);
API_DLL void set_object_polygon(int contour_count, int *len, double *position_buffer);
API_DLL void push_find_difference();
API_DLL void push_find_intersection();
API_DLL void push_find_union();
API_DLL int peek_result_size();
API_DLL int peek_result_polygon_contour_count();
API_DLL int peek_result_polygon_buffer_size();
API_DLL void pop_result_polygon(int id, int *len, double *position_buffer);

#endif //GEOMETRY_LIBRARY_REGION_API_H
