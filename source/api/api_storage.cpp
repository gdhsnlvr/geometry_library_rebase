//
// Created by gdhsnlvr on 20.01.17.
//

#include "api_storage.h"

api_storage::api_storage()
{
    m_deposit = new Deposit();
}

api_storage::~api_storage()
{
    if (m_deposit)
        delete m_deposit;
}

Deposit *api_storage::deposit()
{
    return m_deposit;
}

const geometry::mesh &api_storage::get_model(int index) const
{
    if (index < m_deposit->horizons().size())
        return m_deposit->horizons()[index]->mesh();
    return m_deposit->surface();
}

const geometry::quad_mesh &api_storage::get_model_patches(int index) const
{
    if (index < m_deposit->horizons().size())
        return m_deposit->horizons()[index]->mesh_patches();
    return m_deposit->surface_patches();
}

int api_storage::get_model_count() const
{
    return m_deposit ? (int) m_deposit->horizons().size() : 0;
}

void api_storage::set_model_patches(int id, const geometry::quad_mesh &patched_mesh)
{
    if (id < m_deposit->horizons().size())
        m_deposit->horizons()[id]->setMeshPatches(patched_mesh);
    else if (id == m_deposit->horizons().size())
        m_deposit->setSurfacePatches(patched_mesh);
}

api_storage &api_storage::instance()
{
    static api_storage storage;
    return storage;
}
