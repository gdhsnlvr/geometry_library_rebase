//
// Created by gdhsnlvr on 20.01.17.
//

#ifndef GEOMETRY_LIBRARY_API_H
#define GEOMETRY_LIBRARY_API_H

#include "../geometry/real.h"

#ifdef __linux__
#define API_DLL
#else
#define API_DLL extern "C" __declspec(dllexport)
#endif

API_DLL int pick_model(real_t bx, real_t by, real_t bz,
                       real_t ex, real_t ey, real_t ez);

API_DLL int is_surface(int id);
API_DLL int is_horizont(int id);

#endif //GEOMETRY_LIBRARY_API_H
