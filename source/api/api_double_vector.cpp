//
// Created by gdhsnlvr on 27.05.17.
//

#include "api_double_vector.h"
#include "api.h"

api_double_vector::api_double_vector(const std::vector<double> &v)
    : _vector(v)
{

}

api_double_vector::api_double_vector(double *array, int n)
    : _vector(array, array + n)
{

}

double api_double_vector::get(int i) const {
    return _vector[i];
}

void api_double_vector::set(int i, double value) {
    _vector[i] = value;
}

int api_double_vector::size() const {
    return (int) _vector.size();
}

API_DLL api_double_vector * api_double_vector_new(double *data, int size) {
    return new api_double_vector(data, size);
}

API_DLL double api_double_vector_get(api_double_vector * obj, int i) {
    return obj->get(i);
}

API_DLL void api_double_vector_set(api_double_vector * obj, int i, double value) {
    obj->set(i, value);
}

API_DLL int api_double_vector_size(api_double_vector * obj) {
    return obj->size();
}

API_DLL void api_double_vector_free(api_double_vector * obj) {
    delete obj;
}