//
// Created by gdhsnlvr on 02.06.17.
//

#ifndef GEOMETRY_LIBRARY_GEOMETRY_API_H
#define GEOMETRY_LIBRARY_GEOMETRY_API_H


#include "api_double_vector.h"
#include "api.h"

#include "../geometry/mesh.h"
#include "../geometry/polygon.h"
#include "api_vec2_vector.h"
#include "api_polygon_vector.h"

API_DLL geometry::mesh* api_build_surface(double *x, double *y, double *z, int count);
API_DLL geometry::mesh* api_build_horizon(geometry::mesh *mesh, double from, double to);
API_DLL geometry::mesh* api_build_cross(geometry::mesh *mesh, double *x, double *y, int n);
API_DLL geometry::mesh* api_cut_mesh(geometry::mesh *mesh, geometry::polygon *polygon);

API_DLL double api_ray_pick_parameter(geometry::mesh *mesh,
                                      double px, double py, double pz,
                                      double ex, double ey, double ez);

API_DLL api_double_vector* api_position_buffer(geometry::mesh *mesh);
API_DLL api_double_vector* api_normal_buffer(geometry::mesh *mesh);
API_DLL api_double_vector* api_color_buffer(geometry::mesh *mesh);

// vec2
API_DLL geometry::vec2* api_vec2_new(double x, double y);
API_DLL double api_vec2_x(geometry::vec2* obj);
API_DLL double api_vec2_y(geometry::vec2* obj);
API_DLL void api_vec2_free(geometry::vec2* obj);

// polygon
API_DLL geometry::polygon* api_polygon_new(api_vec2_vector* contour);
API_DLL api_vec2_vector* api_polygon_outer(geometry::polygon* polygon);
API_DLL api_vec2_vector* api_polygon_inner(geometry::polygon* polygon, int index);
API_DLL int api_polygon_inner_count(geometry::polygon* polygon);
API_DLL void api_polygon_free(geometry::polygon* polygon);

API_DLL api_polygon_vector* api_polygon_unite(geometry::polygon *a, geometry::polygon *b);
API_DLL api_polygon_vector* api_polygon_intersect(geometry::polygon *a, geometry::polygon *b);
API_DLL api_polygon_vector* api_polygon_difference(geometry::polygon *a, geometry::polygon *b);
API_DLL geometry::mesh* api_polygon_build_mesh(geometry::polygon *polygon);

#endif //GEOMETRY_LIBRARY_GEOMETRY_API_H
