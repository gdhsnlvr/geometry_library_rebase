//
// Created by gdhsnlvr on 03.06.17.
//

#ifndef GEOMETRY_LIBRARY_API_VEC2_VECTOR_H
#define GEOMETRY_LIBRARY_API_VEC2_VECTOR_H


#include <vector>
#include "../geometry/vec2.h"

class api_vec2_vector {
protected:
    std::vector<geometry::vec2*> _vector;

public:
    api_vec2_vector(const std::vector<geometry::vec2*> &v);
    api_vec2_vector(const std::vector<geometry::vec2> &v);
    api_vec2_vector(geometry::vec2 **array, int n);

    geometry::vec2* get(int i) const;
    void set(int i, geometry::vec2 *value);
    int size() const;

    std::vector<geometry::vec2> points() const;
};


#endif //GEOMETRY_LIBRARY_API_VEC2_VECTOR_H
