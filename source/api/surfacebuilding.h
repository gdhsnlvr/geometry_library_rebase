//
// Created by User on 13.10.2016.
//

#ifndef GEOMETRY_LIBRARY_SURFACEBUILDING_H
#define GEOMETRY_LIBRARY_SURFACEBUILDING_H

#include "api.h"

API_DLL void build_surface(double *x, double *y, double *z, int count);
API_DLL void build_horizons(double *h_begin, double *h_end, int count);

API_DLL void build_model_patches(int id, int patch_size, int patch_count);

API_DLL void load_surface(const char *filePath);
API_DLL void load_horizons(const char *filePath);

API_DLL void save_surface(const char *filePath);
API_DLL void save_horizons(const char *filePath);

#endif //GEOMETRY_LIBRARY_SURFACEBUILDING_H