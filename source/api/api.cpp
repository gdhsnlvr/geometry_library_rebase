//
// Created by gdhsnlvr on 29.01.17.
//

#include "api.h"
#include "api_storage.h"
#include "../geometry/detail/geometry_detail.h"

int pick_model(real_t bx, real_t by, real_t bz,
               real_t ex, real_t ey, real_t ez)
{
    geometry::vec3 begin(bx, by, bz);
    geometry::vec3 end(ex, ey, ez);

    int index = -1;
    real_t parameter = geometry::real::neg_infinity;
    for (int i = 0; i < api_storage::instance().get_model_count(); i++) {
        real_t current_parameter = geometry::detail::rayCasting(
                api_storage::instance().get_model(i),
                geometry::line<geometry::vec3>(begin, end)
        );

        if (current_parameter > parameter) {
            parameter = current_parameter;
            index = i;
        }
    }

    return index;
}

int is_surface(int id)
{
    return id == (int) api_storage::instance().deposit()->horizons().size();
}

int is_horizont(int id)
{
    return id < (int) api_storage::instance().deposit()->horizons().size();
}
