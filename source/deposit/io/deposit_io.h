//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOMETRY_LIBRARY_DEPOSIT_IO_H
#define GEOMETRY_LIBRARY_DEPOSIT_IO_H


#include <fstream>
#include "../Deposit.h"

namespace deposit
{
    namespace io
    {
        void write(Deposit *deposit, const std::string &fileName);

        void write(Horizon *horizon, const std::string &fileName);
    }
}


#endif //GEOMETRY_LIBRARY_DEPOSIT_IO_H
