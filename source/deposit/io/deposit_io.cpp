//
// Created by gdhsnlvr on 25.09.16.
//

#include "deposit_io.h"
#include "../../geometry/io/detail/geometry_io_detail.h"

void deposit::io::write(Deposit *deposit, const std::string &fileName)
{
    std::ofstream ofs(fileName, std::ios_base::binary);
    geometry::io::detail::write(deposit->horizons().size(), ofs);
    for (Horizon *horizon : deposit->horizons()) {
        geometry::io::detail::write(horizon->mesh(), ofs);
    }
}

void deposit::io::write(Horizon *horizon, const std::string &fileName)
{
    std::ofstream ofs(fileName, std::ios_base::binary);
    geometry::io::detail::write(horizon->mesh(), ofs);
}