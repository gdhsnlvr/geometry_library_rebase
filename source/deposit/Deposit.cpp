//
// Created by gdhsnlvr on 25.09.16.
//

#include <algorithm>
#include "Deposit.h"

#include "../geometry/detail/geometry_detail.h"

Deposit::Deposit()
{
}

Deposit::Deposit(const geometry::mesh &mesh)
{
    setSurface(mesh);
}

Deposit::Deposit(const geometry::mesh &mesh,
                 const std::vector<real_t> &height_begin,
                 const std::vector<real_t> &height_end)
{
    setSurface(mesh);
    buildHorizons(height_begin, height_end);
}

Deposit::~Deposit()
{
    for (Horizon *horizon : m_horizons)
        delete horizon;
}

void Deposit::buildHorizons(const std::vector<real_t> &height_begin,
                            const std::vector<real_t> &height_end)
{
    for (int i = 0; i < height_begin.size(); i++) {
        Horizon *horizon = new Horizon(height_begin[i], height_end[i]);
        horizon->setMesh(geometry::detail::slice_mesh(m_surface, height_begin[i], height_end[i]));
        addHorizon(horizon);
    }
}

void Deposit::setSurface(const geometry::mesh &surface)
{
    m_surface = surface;
}

const geometry::mesh &Deposit::surface() const
{
    return m_surface;
}

void Deposit::setSurfacePatches(const geometry::quad_mesh &surface_patches)
{
    m_surface_patches = surface_patches;
}

const geometry::quad_mesh &Deposit::surface_patches() const
{
    return m_surface_patches;
}

void Deposit::addHorizon(Horizon *horizon)
{
    m_horizons.push_back(horizon);
}

void Deposit::removeHorizon(Horizon *horizon)
{
    auto it = std::find(m_horizons.begin(), m_horizons.end(), horizon);
    if (it != m_horizons.end()) {
        m_horizons.erase(it);
    }
}

const std::vector<Horizon *> &Deposit::horizons() const
{
    return m_horizons;
}
