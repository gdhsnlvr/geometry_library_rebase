//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOMETRY_LIBRARY_DOMAIN_H
#define GEOMETRY_LIBRARY_DOMAIN_H

#include "../geometry/real.h"
#include "../geometry/mesh.h"
#include "../geometry/polygon.h"
#include "../geometry/quad_mesh.h"

class Region;

class Domain
{
protected:
    real_t m_heightMin;
    real_t m_heightMax;
    real_t m_quality;
    geometry::mesh m_mesh;
    geometry::quad_mesh m_mesh_patches;
    geometry::polygon m_polygon;
    Region *m_parent;

public:
    Domain();
    Domain(real_t heightMin, real_t heightMax, real_t quality, const geometry::polygon &polygon);

    void setQuality(real_t quality);
    real_t quality() const;

    void setHeightMin(real_t heightMin);
    real_t heightMin() const;

    void setHeightMax(real_t heightMax);
    real_t heightMax() const;

    void setMesh(const geometry::mesh &mesh);
    const geometry::mesh &mesh() const;

    void setMeshPatches(const geometry::quad_mesh &patches);
    const geometry::quad_mesh &mesh_patches() const;

    void setPolygon(const geometry::polygon &polygon);
    const geometry::polygon &polygon() const;

    Region *parent();
    const Region *parent() const;
    void setParent(Region *parent);
};


#endif //GEOMETRY_LIBRARY_DOMAIN_H
