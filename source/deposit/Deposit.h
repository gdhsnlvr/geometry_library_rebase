//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOMETRY_LIBRARY_DEPOSIT_H
#define GEOMETRY_LIBRARY_DEPOSIT_H


#include "../geometry/mesh.h"
#include "Horizon.h"

class Deposit
{
protected:
    geometry::mesh m_surface;
    geometry::quad_mesh m_surface_patches;
    std::vector<Horizon*> m_horizons;

public:
    Deposit();
    Deposit(const geometry::mesh &mesh);
    Deposit(const geometry::mesh &mesh,
            const std::vector<real_t> &height_begin,
            const std::vector<real_t> &height_end);
    ~Deposit();

    void buildHorizons(const std::vector<real_t> &height_begin,
                       const std::vector<real_t> &height_end);

    void setSurface(const geometry::mesh &surface);
    void setSurfacePatches(const geometry::quad_mesh &surface_patches);
    const geometry::mesh &surface() const;
    const geometry::quad_mesh &surface_patches() const;

    void addHorizon(Horizon *horizon);
    void removeHorizon(Horizon *horizon);
    const std::vector<Horizon*> &horizons() const;
};


#endif //GEOMETRY_LIBRARY_DEPOSIT_H
