//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOMETRY_LIBRARY_HORIZON_H
#define GEOMETRY_LIBRARY_HORIZON_H

#include "../geometry/real.h"
#include "../geometry/mesh.h"
#include "Region.h"
#include "../geometry/quad_mesh.h"

class Horizon
{
protected:
    real_t m_heightMin;
    real_t m_heightMax;
    geometry::mesh m_mesh;
    geometry::quad_mesh m_mesh_patches;
    std::vector<Region*> m_regions;

public:
    Horizon(real_t heightMin, real_t heightMax);
    ~Horizon();

    void setHeightMin(real_t heightMin);
    real_t heightMin() const;

    void setHeightMax(real_t heightMax);
    real_t heightMax() const;

    void setMesh(const geometry::mesh &mesh);
    void setMeshPatches(const geometry::quad_mesh &mesh_patches);
    const geometry::mesh &mesh() const;
    const geometry::quad_mesh &mesh_patches() const;

    void addRegion(Region *region);
    void removeRegion(Region *region);
    const std::vector<Region*> &regions() const;
};


#endif //GEOMETRY_LIBRARY_HORIZON_H
