//
// Created by gdhsnlvr on 25.09.16.
//

#ifndef GEOMETRY_LIBRARY_REGION_H
#define GEOMETRY_LIBRARY_REGION_H


#include "Domain.h"

class Horizon;

class Region
{
protected:
    std::vector<Domain*> m_domains;
    Horizon *m_parent;

public:
    Region() = default;
    Region(const std::vector<Domain*> domains);
    ~Region();

    void addDomain(Domain *domain);
    void removeDomain(Domain *domain);

    const std::vector<Domain*> &domains() const;
    std::vector<Domain*> &r_domains();

    Horizon *parent();
    const Horizon *parent() const;
    void setParent(Horizon *parent);
};


#endif //GEOMETRY_LIBRARY_REGION_H
