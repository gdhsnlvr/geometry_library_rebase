//
// Created by gdhsnlvr on 18.09.16.
//

#include "mesh.h"

geometry::mesh::mesh(const std::vector<geometry::triangle> triangles)
        : m_triangles(triangles)
{
    updateBoundary();
}

void geometry::mesh::updateBoundary()
{
    m_minBoundary = geometry::vec3(geometry::real::infinity);
    m_maxBoundary = geometry::vec3(geometry::real::neg_infinity);
    for (const geometry::triangle &triangle : m_triangles) {
        for (const geometry::vertex &vertex : triangle) {
            m_minBoundary = geometry::vec3::componentMin(m_minBoundary, vertex.position);
            m_maxBoundary = geometry::vec3::componentMax(m_maxBoundary, vertex.position);
        }
    }
}

const std::vector<geometry::triangle> &geometry::mesh::triangles() const
{
    return m_triangles;
}

std::vector<geometry::triangle> &geometry::mesh::r_triangles()
{
    return m_triangles;
}

const geometry::vec3 &geometry::mesh::minBoundary() const
{
    return m_minBoundary;
}

const geometry::vec3 &geometry::mesh::maxBoundary() const
{
    return m_maxBoundary;
}
