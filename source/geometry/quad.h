//
// Created by gdhsnlvr on 07.02.17.
//

#ifndef GEOMETRY_LIBRARY_QUAD_H
#define GEOMETRY_LIBRARY_QUAD_H

#include <array>
#include "vertex.h"

namespace geometry {
    class quad
    {
    protected:
        std::array<vertex, 4> m_vertexes;

    public:
        quad() = default;
        quad(const vertex &v1, const vertex &v2, const vertex &v3, const vertex &v4);

        const vertex &operator [] (std::size_t index) const;
        vertex &operator [] (std::size_t index);

        std::array<vertex, 4>::const_iterator begin() const;
        std::array<vertex, 4>::const_iterator end() const;
        std::array<vertex, 4>::iterator begin();
        std::array<vertex, 4>::iterator end();
    };
}


#endif //GEOMETRY_LIBRARY_QUAD_H
