//
// Created by gdhsnlvr on 07.02.17.
//

#include "quad_mesh.h"

geometry::quad_mesh::quad_mesh(const std::vector<geometry::quad> quads)
    : m_quads(quads)
{
    updateBoundary();
}

const std::vector<geometry::quad> &geometry::quad_mesh::quads() const
{

    return m_quads;
}

std::vector<geometry::quad> &geometry::quad_mesh::r_quads()
{
    return m_quads;
}

const geometry::vec3 &geometry::quad_mesh::minBoundary() const
{
    return m_minBoundary;
}

const geometry::vec3 &geometry::quad_mesh::maxBoundary() const
{
    return m_maxBoundary;
}

void geometry::quad_mesh::updateBoundary()
{
    m_minBoundary = geometry::vec3(geometry::real::infinity);
    m_maxBoundary = geometry::vec3(geometry::real::neg_infinity);
    for (const geometry::quad &quad : m_quads) {
        for (const geometry::vertex &vertex : quad) {
            m_minBoundary = geometry::vec3::componentMin(m_minBoundary, vertex.position);
            m_maxBoundary = geometry::vec3::componentMax(m_maxBoundary, vertex.position);
        }
    }
}
