//
// Created by gdhsnlvr on 01.10.16.
//

#include "horizon_rastarization.h"

geometry::horizon_rastarization::horizon_rastarization()
{

}

const geometry::horizon_rastarization::table_t &geometry::horizon_rastarization::rasters() const
{
    return m_rasters;
}

geometry::horizon_rastarization::table_t &geometry::horizon_rastarization::r_rasters()
{
    return m_rasters;
}
