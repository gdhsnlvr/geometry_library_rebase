//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOMETRY_LIBRARY_LINE_H
#define GEOMETRY_LIBRARY_LINE_H


#include "vec2.h"

namespace geometry {
    template <class vec>
    class line {
    protected:
        vec m_begin;
        vec m_end;

    public:
        line() = default;
        line(const vec &v1, const vec &v2)
                : m_begin(v1), m_end(v2)
        {
        }

        const vec &begin() const
        {
            return m_begin;
        }

        const vec &end() const
        {
            return m_end;
        }

        real_t constant() const
        {
            return - vec::dot(vec::normal(m_begin, m_end), m_begin);
        }
    };
}


#endif //GEOMETRY_LIBRARY_LINE_H
