#include "delaunay_triangulation.h"
#include "triangle.h"

geometry::delaunay_triangulation::delaunayTriangle geometry::delaunay_triangulation::make_triangle(int i1, int i2, int i3) const {
    geometry::vec2 v1 = m_buffer[i1] - m_buffer[i2];
    geometry::vec2 v2 = m_buffer[i2] - m_buffer[i3];

    if (geometry::real::fizzyGreater(geometry::vec2::cross(v1, v2), 0)) {
        std::swap(i1, i3);
    }

    return {i1, i2, i3};
}

geometry::delaunay_triangulation::delaunayEdge geometry::delaunay_triangulation::make_edge(int i1, int i2) const {
    if (i1 > i2) {
        std::swap(i1, i2);
    }

    return {i1, i2};
}

geometry::delaunay_triangulation::delaunay_triangulation(const std::vector<geometry::vec2> &initialPoints)
    : m_buffer(initialPoints), m_count(initialPoints.size())
{
    geometry::vec2 min(geometry::real::infinity);
    geometry::vec2 max(geometry::real::neg_infinity);

    for (const geometry::vec2 &point : m_buffer) {
        min = geometry::vec2::componentMin(min, point);
        max = geometry::vec2::componentMax(max, point);
    }

    real_t width = max.x - min.x;
    real_t height = max.y - min.y;

    m_buffer.push_back(geometry::vec2(min.x - 4.0f * width, min.y - height));
    m_buffer.push_back(geometry::vec2(max.x + width, min.y - height));
    m_buffer.push_back(geometry::vec2(max.x + width, max.y + 4.0f * height));

    m_superTriangle = make_triangle(m_count, m_count + 1, m_count + 2);
    m_triangles.insert(m_triangles.end(), m_superTriangle.begin(), m_superTriangle.end());
}

void geometry::delaunay_triangulation::insert(int index) {
    std::map<delaunayEdge, int> edges;

    for (int i = 0; i < m_triangles.size(); i += 3) {
        int p1 = i, p2 = i + 1, p3 = i + 2;
        auto triangle = make_triangle(m_triangles[p1], m_triangles[p2], m_triangles[p3]);

        if (insideCircumcircle(triangle[0], triangle[1], triangle[2], index)) {
            edges[make_edge(triangle[0], triangle[1])]++;
            edges[make_edge(triangle[1], triangle[2])]++;
            edges[make_edge(triangle[2], triangle[0])]++;
            m_triangles[p1] = m_triangles[m_triangles.size() - 3];
            m_triangles[p2] = m_triangles[m_triangles.size() - 2];
            m_triangles[p3] = m_triangles[m_triangles.size() - 1];
            m_triangles.resize(m_triangles.size() - 3);
            i -= 3;
        }
    }

    for (auto it = edges.begin(); it != edges.end(); it++) {
        if (it->second == 1) {
            auto triangle = make_triangle(it->first[0], it->first[1], index);
            for (int i = 0; i < 3; i++)
                m_triangles.push_back(triangle[i]);
        }
    }
}

bool geometry::delaunay_triangulation::insideCircumcircle(int i1, int i2, int i3, int p) const {
    geometry::vec2 p1 = m_buffer[i1];
    geometry::vec2 p2 = m_buffer[i2];
    geometry::vec2 p3 = m_buffer[i3];
    geometry::vec2 d = m_buffer[p];

    long double ab = (p1.x * p1.x) + (p1.y * p1.y);
    long double cd = (p2.x * p2.x) + (p2.y * p2.y);
    long double ef = (p3.x * p3.x) + (p3.y * p3.y);

    long double circum_x = (ab * (p3.y - p2.y) + cd * (p1.y - p3.y) + ef * (p2.y - p1.y))
                      / (p1.x * (p3.y - p2.y) + p2.x * (p1.y - p3.y) + p3.x * (p2.y - p1.y)) * 0.5;
    long double circum_y = (ab * (p3.x - p2.x) + cd * (p1.x - p3.x) + ef * (p2.x - p1.x))
                      / (p1.y * (p3.x - p2.x) + p2.y * (p1.x - p3.x) + p3.y * (p2.x - p1.x)) * 0.5;

    long double dist = std::sqrt(((d.x - circum_x) * (d.x - circum_x)) + ((d.y - circum_y) * (d.y - circum_y)));
    long double circum_radius = std::sqrt(((p1.x - circum_x) * (p1.x - circum_x)) + ((p1.y - circum_y) * (p1.y - circum_y)));

    return dist <= circum_radius;
}

void geometry::delaunay_triangulation::build() {
    for (int i = 0; i < m_count; i++) {
        insert(i);
    }
}

std::vector<int> geometry::delaunay_triangulation::result() const {
    std::vector<int> triangles;
    for (int i = 0; i < m_triangles.size(); i += 3) {
        int p1 = i, p2 = i + 1, p3 = i + 2;
        auto triangle = make_triangle(m_triangles[p1], m_triangles[p2], m_triangles[p3]);

        if (std::find(triangle.begin(), triangle.end(), m_superTriangle[0]) != triangle.end()) continue;
        if (std::find(triangle.begin(), triangle.end(), m_superTriangle[1]) != triangle.end()) continue;
        if (std::find(triangle.begin(), triangle.end(), m_superTriangle[2]) != triangle.end()) continue;

        triangles.insert(triangles.end(), triangle.begin(), triangle.end());
    }
    return triangles;
}
