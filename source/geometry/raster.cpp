//
// Created by gdhsnlvr on 03.10.16.
//

#include "raster.h"

geometry::raster::raster()
{
    m_stoneVolume = 0;
}

real_t geometry::raster::stoneVolume() const
{
    return m_stoneVolume;
}

void geometry::raster::setStoneVolume(real_t volume)
{
    m_stoneVolume = volume;
}
