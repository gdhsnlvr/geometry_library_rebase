//
// Created by gdhsnlvr on 19.09.16.
//

#include "plane.h"

geometry::plane::plane(const geometry::vec3 &normal, real_t constant)
        : m_normal(normal), m_constant(constant)
{

}

geometry::plane::plane(const geometry::vec3 &a, const geometry::vec3 &b, const geometry::vec3 &c)
{
    m_normal = geometry::vec3::normal(a, b, c);
    m_constant = - geometry::vec3::dot(m_normal, a);
}

const geometry::vec3 &geometry::plane::normal() const
{
    return m_normal;
}

real_t geometry::plane::constant() const
{
    return m_constant;
}
