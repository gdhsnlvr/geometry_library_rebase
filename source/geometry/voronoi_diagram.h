//
// Created by gdhsnlvr on 19.01.17.
//

#ifndef GEOMETRY_LIBRARY_VORONOI_DIAGRAM_H
#define GEOMETRY_LIBRARY_VORONOI_DIAGRAM_H


#include <vector>

#include "vec2.h"

namespace geometry {
    enum voronoi_edge_type
    {
        finite,
        infinite_begin,
        infinite_end
    };

    struct voronoi_edge
    {
        int from;
        int to;
        int owner;

        voronoi_edge *reversed;
        voronoi_edge *next;

        voronoi_edge_type type;
    };

    struct voronoi_locus
    {
        int index;
        voronoi_edge *list_head;
    };

    class voronoi_diagram
    {
    protected:
        std::vector<vec2> m_cites;
        std::vector<vec2> m_points;
        std::vector<voronoi_locus> m_locuses;

    public:
        voronoi_diagram() = default;
        ~ voronoi_diagram();

        voronoi_locus find_any_infinity_locus() const;
        std::vector<int> find_convex_hull() const;
        int split_edge_with_reversed_by_point(voronoi_edge *edge, vec2 point);
    };
}


#endif //GEOMETRY_LIBRARY_VORONOI_DIAGRAM_H
