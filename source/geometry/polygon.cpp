//
// Created by gdhsnlvr on 24.09.16.
//

#include "polygon.h"
#include "gpc/gpc.h"

geometry::polygon::polygon(const geometry::polygon::contour_t &contour)
    : m_outer(contour) {

}

geometry::polygon::polygon(const geometry::polygon::contour_t &outer,
                           const std::vector<geometry::polygon::contour_t> &inners)
    : m_outer(outer), m_inners(inners) {

}

void geometry::polygon::set_outer(const geometry::polygon::contour_t &contour) {
    m_outer = contour;
}

void geometry::polygon::add_inner(const geometry::polygon::contour_t &contour) {
    m_inners.push_back(contour);
}

const geometry::polygon::contour_t &geometry::polygon::outer() const {
    return m_outer;
}

const std::vector<geometry::polygon::contour_t> &geometry::polygon::inners() const {
    return m_inners;
}
