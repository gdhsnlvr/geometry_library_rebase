//
// Created by gdhsnlvr on 19.09.16.
//

#include <iostream>
#include "geometry_io_detail.h"

namespace geometry
{
    namespace io
    {
        namespace detail
        {
            void read(geometry::vec2 &vec, std::ifstream &ifs)
            {
                read(vec.x, ifs);
                read(vec.y, ifs);
            }

            void read(geometry::vec3 &vec, std::ifstream &ifs)
            {
                read(vec.x, ifs);
                read(vec.y, ifs);
                read(vec.z, ifs);
            }

            void read(geometry::vertex &vertex, std::ifstream &ifs)
            {
                read(vertex.position, ifs);
                read(vertex.normal, ifs);
                read(vertex.color, ifs);
            }

            void read(geometry::triangle &triangle, std::ifstream &ifs)
            {
                for (geometry::vertex &vertex : triangle) {
                    read(vertex, ifs);
                }
            }

            void read(geometry::mesh &mesh, std::ifstream &ifs)
            {
                int count; read(count, ifs);
                mesh.r_triangles().resize(count);
                for (geometry::triangle &triangle : mesh.r_triangles()) {
                    read(triangle, ifs);
                }
            }

            void write(const geometry::vec2 &vec, std::ofstream &ofs)
            {
                write(vec.x, ofs);
                write(vec.y, ofs);
            }

            void write(const geometry::vec3 &vec, std::ofstream &ofs)
            {
                write(vec.x, ofs);
                write(vec.y, ofs);
                write(vec.z, ofs);
            }

            void write(const geometry::vertex &vertex, std::ofstream &ofs)
            {
                write(vertex.position, ofs);
                write(vertex.normal, ofs);
                write(vertex.color, ofs);
            }

            void write(const geometry::triangle &triangle, std::ofstream &ofs)
            {
                for (const geometry::vertex &vertex : triangle) {
                    write(vertex, ofs);
                }
            }

            void write(const geometry::mesh &mesh, std::ofstream &ofs)
            {
                write((int)mesh.triangles().size(), ofs);
                for (const geometry::triangle &triangle : mesh.triangles()) {
                    write(triangle, ofs);
                }
            }
        }
    }
}