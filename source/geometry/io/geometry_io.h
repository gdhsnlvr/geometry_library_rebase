//
// Created by gdhsnlvr on 19.09.16.
//

#ifndef GEOMETRY_LIBRARY_IO_H
#define GEOMETRY_LIBRARY_IO_H

#include <string>
#include <fstream>

#include "../mesh.h"

#include "detail/geometry_io_detail.h"

namespace geometry
{
    namespace io
    {
        template <typename T>
        T read(const std::string &fileName)
        {
            std::ifstream ifs(fileName, std::ios_base::binary);

            T temp;
            geometry::io::detail::read(temp, ifs);
            return temp;
        }


        template <typename T>
        void write(const T &t, const std::string &fileName)
        {
            std::ofstream ofs(fileName, std::ios_base::binary);
            geometry::io::detail::write(t, ofs);
        }
    }
}


#endif //GEOMETRY_LIBRARY_IO_H
