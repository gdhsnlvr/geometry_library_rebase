//
// Created by gdhsnlvr on 18.09.16.
//

#include "vertex.h"

geometry::vertex::vertex(const geometry::vec3 &position, const geometry::vec3 &normal, const geometry::vec3 &color)
        : position(position), normal(normal), color(color)
{
}

int geometry::vertex::positionOffset() {
    return offsetof(geometry::vertex, position);
}

int geometry::vertex::normalOffset() {
    return offsetof(geometry::vertex, normal);
}

int geometry::vertex::colorOffset() {
    return offsetof(geometry::vertex, color);
}

int geometry::vertex::stride() {
    return sizeof(geometry::vertex);
}
