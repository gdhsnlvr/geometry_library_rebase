//
// Created by gdhsnlvr on 04.10.16.
//

#ifndef GEOMETRY_LIBRARY_MAPED_MESH_H
#define GEOMETRY_LIBRARY_MAPED_MESH_H


#include <vector>

namespace geometry
{
    class mapped_mesh
    {
    protected:
        std::vector<std::vector<std::vector<int> > > m_map;

    public:
        mapped_mesh() = default;

        const std::vector<std::vector<std::vector<int> > > &map() const;
        std::vector<std::vector<std::vector<int> > > &r_map();
    };
}


#endif //GEOMETRY_LIBRARY_MAPED_MESH_H
