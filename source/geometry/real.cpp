//
// Created by gdh on 24.07.16.
//


#include <cmath>

#include "real.h"

bool geometry::real::fizzyEqual(real_t a, real_t b) {
    return std::abs(a - b) < epsilon;
}

bool geometry::real::fizzyLess(real_t a, real_t b) {
    return a < b + epsilon;
}

bool geometry::real::fizzyGreater(real_t a, real_t b) {
    return a + epsilon > b;
}

real_t geometry::real::abs(real_t x)
{
    return x >= 0 ? x : - x;
}

real_t geometry::real::max(real_t a, real_t b)
{
    return a > b ? a : b;
}

real_t geometry::real::min(real_t a, real_t b)
{
    return a < b ? a : b;
}
