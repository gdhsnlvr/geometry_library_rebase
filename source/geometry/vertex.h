//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOMETRY_LIBRARY_VERTEX_H
#define GEOMETRY_LIBRARY_VERTEX_H

#include <cstddef>
#include "vec3.h"

namespace geometry {
    struct vertex {
        vec3 position;
        vec3 normal;
        vec3 color;

        vertex() = default;
        vertex(const vec3 &position, const vec3 &normal, const vec3 &color);

        static int positionOffset();
        static int normalOffset();
        static int colorOffset();
        static int stride();
    };
}


#endif //GEOMETRY_LIBRARY_VERTEX_H
