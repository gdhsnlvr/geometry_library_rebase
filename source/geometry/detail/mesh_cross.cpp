//
// Created by gdhsnlvr on 27.05.17.
//

#include "mesh_cross.h"
#include "geometry_detail.h"

std::vector<geometry::vec2> geometry::get_heights_on_segment(const geometry::mesh &mesh,
                                                     const geometry::segment &segment)
{
    std::vector<geometry::vec2> heights;
    double height_begin = geometry::detail::heightAt(segment.begin(), mesh);
    double height_end = geometry::detail::heightAt(segment.end(), mesh);

    if (height_begin > 0) {
        heights.emplace_back(0, height_begin);
    }

    for (auto triangle : mesh.triangles()) {
        for (int i = 0; i < 3; i++) {
            int j = (i + 1) % 3;

            geometry::vec2 beg = triangle[i].position;
            geometry::vec2 end = triangle[j].position;
            geometry::segment edge = {beg, end};
            std::pair<double, double> params;
            if (geometry::detail::intersect(segment, edge, params)) {
                double hbeg = triangle[i].position.z;
                double hend = triangle[j].position.z;
                double height = hbeg + params.second * (hend - hbeg);
                heights.emplace_back(params.first, height);
            }
        }
    }

    if (height_end > 0) {
        heights.emplace_back(1, height_end);
    }

    return heights;
}

std::vector<geometry::vec2> geometry::get_heights_on_cross(const geometry::mesh &mesh,
                                                   const std::vector<geometry::vec2> &points)
{
    std::vector<geometry::vec2> heights;

    double max_length = 0;
    for (int i = 1; i < points.size(); i++) {
        max_length = std::max(max_length, geometry::detail::distance(points[i], points[i-1]));
    }

    double sum_length = 0;
    for (int i = 1; i < points.size(); i++) {
        geometry::segment segment = {points[i-1], points[i]};
        auto local_height = geometry::get_heights_on_segment(mesh, segment);
        auto local_length = geometry::detail::distance(points[i], points[i-1]);
        for (auto &height : local_height) {
            height.x *= local_length / max_length;
            sum_length += local_length / max_length;
            if (!heights.empty()) {
                height.x += heights.back().x;
            }
        }
        heights.insert(heights.end(), local_height.begin(), local_height.end());
    }

    for (auto &height : heights) {
        height.x /= sum_length;
    }

    return heights;
}
