//
// Created by gdhsnlvr on 22.01.17.
//

#include "unite_convex_hull.h"
#include "geometry_detail.h"

namespace geometry {

#define prev_vertex(i, n) (i - 1 + (int) n) % (int) n
#define next_vertex(i, n) (i + 1) % (int) n

std::pair<int, int>
find_lower_bridge(const std::vector<vec2> &points,
                  const std::vector<int> &i_left,
                  const std::vector<int> &i_right)
{
    int l = 0, r = 0;

    for (int i = 0; i < i_left.size(); i++)
        if (points[i_left[i]].x > points[i_left[l]].x)
            l = i;

    for (int i = 0; i < i_right.size(); i++)
        if (points[i_right[i]].x < points[i_right[r]].x)
            r = i;

    auto is_less = [&l, &r, &points, &i_left, &i_right] (int p) {
        return detail::subs_in_line(line<vec2>(points[i_left[l]], points[i_right[r]]), points[p]) < 0;
    };

    bool relax = true;

    while (relax) {
        relax = false;
        while (is_less(i_left[prev_vertex(l, i_left.size())]))
            l = prev_vertex(l, i_left.size()), relax = true;
        while (is_less(i_right[next_vertex(r, i_right.size())]))
            r = next_vertex(r, i_right.size()), relax = true;
    }

    return std::make_pair(i_left[l], i_right[r]);
}

std::pair<int, int>
find_upper_bridge(const std::vector<vec2> &points,
                  const std::vector<int> &i_left,
                  const std::vector<int> &i_right)
{
    int l = 0, r = 0;

    for (int i = 0; i < i_left.size(); i++)
        if (points[i_left[i]].x > points[i_left[l]].x)
            l = i;

    for (int i = 0; i < i_right.size(); i++)
        if (points[i_right[i]].x < points[i_right[r]].x)
            r = i;

    auto is_less = [&l, &r, &points, &i_left, &i_right] (int p) {
        return detail::subs_in_line(line<vec2>(points[i_left[l]], points[i_right[r]]), points[p]) > 0;
    };

    bool relax = true;

    while (relax) {
        relax = false;
        while (is_less(i_left[next_vertex(l, i_left.size())]))
            l = next_vertex(l, i_left.size()), relax = true;
        while (is_less(i_right[prev_vertex(r, i_right.size())]))
            r = prev_vertex(r, i_right.size()), relax = true;
    }

    return std::make_pair(i_left[l], i_right[r]);
}

std::vector<std::pair<int, int>>
find_bridges(const std::vector<vec2> &points,
             const std::vector<int> &i_left,
             const std::vector<int> &i_right)
{
    std::vector<std::pair<int, int>> result;

    auto lower_bridge = find_lower_bridge(points, i_left, i_right);
    auto upper_bridge = find_upper_bridge(points, i_left, i_right);

    double cross_1 = geometry::vec2::cross(
            points[lower_bridge.second] - points[lower_bridge.first],
            points[upper_bridge.first] - points[lower_bridge.first]
    );

    double cross_2 = geometry::vec2::cross(
            points[lower_bridge.second] - points[lower_bridge.first],
            points[upper_bridge.second] - points[lower_bridge.first]
    );

    if (real::abs(cross_1) < real::epsilon && real::abs(cross_2) < real::epsilon) {
        result.push_back(lower_bridge);
        return result;
    }

    result.push_back(lower_bridge);
    result.push_back(upper_bridge);
    return result;
}

#undef prev_vertex
#undef next_vertex

}