//
// Created by gdhsnlvr on 22.01.17.
//

#include "find_convex_hull_test.h"
#include "geometry_detail.h"

namespace geometry {

std::vector<vec2>
find_convex_hull(const std::vector<vec2> &points)
{
    std::vector<vec2> sorted = geometry::detail::sort_by_angle(points);
    std::vector<vec2> result; int n;
    for (const vec2 &p : sorted) {
        while((n = (int) result.size()) >= 2 && vec2::cross(result[n-1] - result[n-2], p - result[n-1]) <= 0) {
            result.pop_back();
        }
        result.push_back(p);
    }
    return result;
}

}