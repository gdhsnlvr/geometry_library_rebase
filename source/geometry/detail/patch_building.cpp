//
// Created by gdhsnlvr on 07.02.17.
//

#include "patch_building.h"
#include "../mapped_mesh.h"
#include "geometry_detail.h"

geometry::quad_mesh geometry::build_patched_mesh(const geometry::mesh &mesh,
                                                 const geometry::vec2 &size,
                                                 int detalization)
{
    geometry::mapped_mesh mapped_mesh = geometry::detail::mapMesh(mesh, size / detalization);

    auto containingRaster = [](real_t x, real_t size) {
        return (std::size_t) std::floor(x / size);
    };

    auto getRasterPosition = [](std::size_t i, std::size_t j, const geometry::vec2 &size) {
        return geometry::vec2(i * size.x, j * size.y);
    };

    geometry::vec3 square = mesh.maxBoundary() - mesh.minBoundary();
    std::size_t countX = containingRaster(square.x, size.x) + 1;
    std::size_t countY = containingRaster(square.y, size.y) + 1;

    geometry::vec2 gridBegin = mesh.minBoundary();

    geometry::vec2 detalizationSize = size / detalization;
    geometry::quad_mesh result;

    for (std::size_t i = 0; i < countX; i++) {
        for (std::size_t j = 0; j < countY; j++) {
            geometry::vec2 rasterPosition = gridBegin + getRasterPosition(i, j, size);
            std::vector<geometry::vertex> points;

            for (std::size_t id = 0; id <= detalization; id++) {
                for (std::size_t jd = 0; jd <= detalization; jd++) {
                    geometry::vec2 point = rasterPosition + getRasterPosition(id, jd, detalizationSize);
                    if (point.x > mesh.maxBoundary().x || point.x < mesh.minBoundary().x ||
                        point.y > mesh.maxBoundary().y || point.y < mesh.minBoundary().y) {
                        continue;
                    }

                    geometry::vec2 offset = point - mesh.minBoundary();
                    std::size_t mappedX = containingRaster(offset.x, detalizationSize.x);
                    std::size_t mappedY = containingRaster(offset.y, detalizationSize.y);
                    real_t height = geometry::detail::heightAtSubSet(
                            point, mesh, mapped_mesh.map()[mappedX][mappedY]
                    );
                    if (height == real::neg_infinity)
                        height = mesh.minBoundary().z;

                    points.emplace_back(geometry::vec3(point.x, point.y, height),
                                        geometry::vec3(0.0, 0.0, 1.0),
                                        geometry::vec3(1.0, 1.0, 1.0));
                }
            }

            if (points.size() == (detalization + 1) * (detalization + 1)) {
                for (std::size_t id = 0; id < points.size(); id += 4) {
                    result.r_quads().emplace_back(points[id], points[id + 1], points[id + 2], points[id + 3]);
                }
            }
        }
    }

    return result;
}
