//
// Created by gdhsnlvr on 05.02.17.
//

#include "mesh_smoothing.h"
#include "../../lib/OpenNL_psm.h"

#include <vector>

namespace geometry {

geometry::mesh smooth_mesh(const geometry::mesh &mesh)
{
    geometry::mesh result = mesh;

    std::vector<geometry::vec3> points;
    for (const geometry::triangle &triangle : result.triangles()) {
        points.push_back(triangle[0].position);
        points.push_back(triangle[1].position);
        points.push_back(triangle[2].position);
    }

    for (int dim = 0; dim < 3; dim++) {
        nlNewContext();
        nlSolverParameteri(NL_NB_VARIABLES, (int) points.size());
        nlSolverParameteri(NL_LEAST_SQUARES, NL_TRUE);
        nlBegin(NL_SYSTEM);
        nlBegin(NL_MATRIX);

        for (int i = 0; i < points.size(); i++) {
            nlBegin(NL_ROW);
            nlCoefficient(i, 1);
            if (dim == 0)
                nlRightHandSide(points[i].x);
            else if (dim == 1)
                nlRightHandSide(points[i].y);
            else
                nlRightHandSide(points[i].z);
            nlEnd(NL_ROW);
        }

        for (int i = 0; i < points.size(); i += 3) {
            for (int j = 0; j < 3; j++) {
                nlBegin(NL_ROW);
                nlCoefficient(i + j,  1);
                nlCoefficient(i + (j + 1) % 3, -1);
                nlEnd(NL_ROW);
            }
        }

        nlEnd(NL_MATRIX);
        nlEnd(NL_SYSTEM);
        nlSolve();

        for (int i = 0; i < points.size(); i += 3) {
            if (dim == 0)
                points[i].x = nlGetVariable(i);
            else if (dim == 1)
                points[i].y = nlGetVariable(i);
            else
                points[i].z = nlGetVariable(i);
        }
    }

    return result;
}

}