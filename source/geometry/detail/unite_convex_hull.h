//
// Created by gdhsnlvr on 22.01.17.
//

#ifndef GEOMETRY_LIBRARY_UNITE_CONVEX_HULL_H
#define GEOMETRY_LIBRARY_UNITE_CONVEX_HULL_H


#include <vector>
#include "../vec2.h"

namespace geometry {

std::pair<int, int>
find_lower_bridge(const std::vector<vec2> &points,
                  const std::vector<int> &i_left,
                  const std::vector<int> &i_right);

std::pair<int, int>
find_upper_bridge(const std::vector<vec2> &points,
                  const std::vector<int> &i_left,
                  const std::vector<int> &i_right);

std::vector<std::pair<int, int>>
find_bridges(const std::vector<vec2> &points,
             const std::vector<int> &i_left,
             const std::vector<int> &i_right);


}


#endif //GEOMETRY_LIBRARY_UNITE_CONVEX_HULL_H
