//
// Created by gdhsnlvr on 22.01.17.
//

#ifndef GEOMETRY_LIBRARY_UNITE_VORONOI_DIAGRAM_H
#define GEOMETRY_LIBRARY_UNITE_VORONOI_DIAGRAM_H


#include "../voronoi_diagram.h"

namespace geometry {

voronoi_diagram
unite_voronoi_diagram(const voronoi_diagram &d_left,
                      const voronoi_diagram &d_right);

}


#endif //GEOMETRY_LIBRARY_UNITE_VORONOI_DIAGRAM_H
