//
// Created by gdhsnlvr on 07.02.17.
//

#ifndef GEOMETRY_LIBRARY_PATCH_BUILDING_H
#define GEOMETRY_LIBRARY_PATCH_BUILDING_H


#include "../mesh.h"
#include "../quad_mesh.h"

namespace geometry
{

geometry::quad_mesh build_patched_mesh(const geometry::mesh &mesh,
                                       const geometry::vec2 &size,
                                       int detalization);

}


#endif //GEOMETRY_LIBRARY_PATCH_BUILDING_H
