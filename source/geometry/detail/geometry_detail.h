//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOMETRY_LIBRARY_GEOMETRY_DETAIL_H
#define GEOMETRY_LIBRARY_GEOMETRY_DETAIL_H

#include "../mesh.h"
#include "../line.h"
#include "../segment.h"
#include "../plane.h"
#include "../horizon_rastarization.h"
#include "../mapped_mesh.h"
#include "../delaunay_triangulation.h"

#include "../../deposit/Horizon.h"

namespace geometry
{
    namespace detail
    {
        real_t distance(const geometry::vec2 &v1, const geometry::vec2 &v2);
        real_t distance(const geometry::vec2 &v, const geometry::segment &segment);
        real_t distance(const geometry::vec3 &v1, const geometry::vec3 &v2);

        real_t square(const geometry::triangle &triangle);

        real_t heightAt(const geometry::vec2 &position, const geometry::plane &plane);
        real_t heightAt(const geometry::vec2 &position, const geometry::triangle &triangle);
        real_t heightAt(const geometry::vec2 &position, const geometry::mesh &mesh);
        real_t heightAtSubSet(const geometry::vec2 &position, const geometry::mesh &mesh, const std::vector<int> &subset);

        real_t subs_in_line(const geometry::line<geometry::vec2> &line, const geometry::vec2 &vec);

        geometry::vec2 normal(const geometry::line<geometry::vec2> &line);
        geometry::vec2 normal(const geometry::segment &segment);
        geometry::vec3 normal(const geometry::plane &plane);
        geometry::vec3 normal(const geometry::triangle &triangle);

        geometry::vec2 center(const geometry::segment &segment);
        geometry::vec3 center(const geometry::triangle &triangle);

        bool intersect(const geometry::line<geometry::vec2> &l1, const geometry::line<geometry::vec2> &l2, std::pair<real_t, real_t> &parameterPack);
        bool intersect(const geometry::line<geometry::vec2> &l1, const geometry::line<geometry::vec2> &l2, vec2 &result);
        bool intersect(const geometry::segment &s1, const geometry::segment &s2, std::pair<real_t, real_t> &parameterPack);
        bool intersect(const geometry::segment &s1, const geometry::segment &s2, vec2 &result);

        bool belongs(const geometry::vec3 &v, const geometry::plane &plane);
        bool belongs(const geometry::vec3 &v, const geometry::triangle &triangle);

        void split_mesh(geometry::mesh &mesh, real_t height);
        void split_mesh(geometry::mesh &mesh, const geometry::segment &segment);
        void split_mesh(geometry::mesh &mesh, const geometry::polygon::contour_t &contour);
        void split_mesh(geometry::mesh &mesh, const geometry::polygon &polygon);

        geometry::mesh slice_mesh(const geometry::mesh &mesh, real_t from, real_t to);
        geometry::mesh cut_mesh(const geometry::mesh &mesh, const geometry::polygon &polygon);

        std::vector<int> buildMeshIndexes(const std::vector<geometry::vec3> &initialPoints);
        geometry::mesh buildMesh(const std::vector<geometry::vec3> &initialPoints);
        geometry::mesh buildMesh(const std::vector<geometry::vec3> &initialPoints,
            const std::vector<int> &indexes);
        geometry::mesh normalizeMesh(const geometry::mesh &mesh, const geometry::vec3 &sizes);

        real_t rayCasting(const geometry::plane &plane, const geometry::line<geometry::vec3> &ray);
        real_t rayCasting(const geometry::triangle &triangle, const geometry::line<geometry::vec3> &ray);
        real_t rayCasting(const geometry::mesh &mesh, const geometry::line<geometry::vec3> &ray);

        geometry::mapped_mesh mapMesh(const geometry::mesh &mesh, const geometry::vec2 &size);
        geometry::horizon_rastarization rastarizate(const Horizon &horizon, const geometry::vec2 &size, int detalization);

        template <typename point_t>
        point_t transform(const geometry::vec2 &vec)
        {
            return point_t(vec.x, vec.y);
        }

        template <typename point_t>
        point_t transform(const geometry::vec3 &vec)
        {
            return point_t(vec.x, vec.y, vec.z);
        }

        std::vector<geometry::vec2> sort_by_angle(const std::vector<geometry::vec2> &points);
        std::vector<int> sort_by_angle(const std::vector<int> &indexes,
                                       const std::vector<geometry::vec2> &points);
    }
}


#endif //GEOMETRY_LIBRARY_GEOMETRY_DETAIL_H
