//
// Created by gdhsnlvr on 11.03.17.
//

#include "polygon_detail.h"
#include "geometry_detail.h"

namespace geometry {
    namespace gpc {
        std::vector<geometry::polygon> split_gpc(const gpc_polygon &polygon) {
            std::vector<geometry::polygon::contour_t> outers;
            std::vector<geometry::polygon::contour_t> inners;
            for (int i = 0; i < polygon.num_contours; i++) {
                geometry::polygon::contour_t contour;
                for (int j = 0; j < polygon.contour[i].num_vertices; j++) {
                    contour.emplace_back(polygon.contour[i].vertex[j].x, polygon.contour[i].vertex[j].y);
                }
                if (polygon.hole[i]) {
                    inners.push_back(contour);
                } else {
                    outers.push_back(contour);
                }
            }
            std::vector<geometry::polygon> result;
            for (const auto &outer : outers) {
                geometry::polygon pol;
                pol.set_outer(outer);
                for (const auto &inner : inners) {
                    if (geometry::belongs(inner[0], outer))
                        pol.add_inner(inner);
                }
                result.push_back(pol);
            }
            return result;
        }

        gpc_polygon to_gpc(const geometry::polygon &polygon) {
            gpc_polygon result;
            result.contour = nullptr;
            result.hole = nullptr;
            result.num_contours = 0;

            gpc_add_contour(result, polygon.outer(), false);
            for (const auto &inner : polygon.inners()) {
                gpc_add_contour(result, inner, true);
            }

            return result;
        }

        void gpc_add_contour(gpc_polygon &polygon, const geometry::polygon::contour_t &contour, bool hole) {
            gpc_vertex_list vertex_list;
            vertex_list.num_vertices = (int) contour.size();
            vertex_list.vertex = new gpc_vertex[contour.size()];
            for (int i = 0; i < contour.size(); i++) {
                vertex_list.vertex[i].x = contour[i].x;
                vertex_list.vertex[i].y = contour[i].y;
            }
            gpc_add_contour(&polygon, &vertex_list, hole);
        }

        geometry::polygon from_gpc(const gpc_polygon &polygon) {
            geometry::polygon::contour_t outer;
            std::vector<geometry::polygon::contour_t> inners;

            for (int i = 0; i < polygon.num_contours; i++) {
                geometry::polygon::contour_t contour;
                for (int j = 0; j < polygon.contour[i].num_vertices; j++) {
                    contour.emplace_back(polygon.contour[i].vertex[j].x, polygon.contour[i].vertex[j].y);
                }
                if (polygon.hole[i])
                    inners.push_back(contour);
                else
                    outer = contour;
            }

            return geometry::polygon(outer, inners);
        }
    }
}

bool geometry::belongs(const geometry::vec2 &vec, const geometry::polygon::contour_t &contour) {
    static geometry::vec2 direction = {55537, 10000007};

    int count_intersection = 0;
    for (int i = 0; i < contour.size(); i++) {
        int j = (i + 1) % (int) contour.size();

        std::pair<real_t, real_t> parameters;
        if (geometry::detail::intersect(
                geometry::line<geometry::vec2>(contour[i], contour[j]),
                geometry::line<geometry::vec2>(vec, vec + direction),
                parameters))
        {
            if (parameters.first > - real::epsilon && parameters.first < 1.0 - real::epsilon && parameters.second > 0) {
                count_intersection++;
            }
        }
    }
    return count_intersection % 2 == 1;
}

bool geometry::belongs(const geometry::vec2 &vec, const geometry::polygon &polygon) {
    bool belongs = geometry::belongs(vec, polygon.outer());
    for (int i = 0; i < polygon.inners().size(); i++) {
        belongs &= !geometry::belongs(vec, polygon.inners().at(i));
    }
    return belongs;
}

geometry::mesh geometry::build_mesh(const geometry::polygon &polygon) {
    std::vector<geometry::vec3> points;
    for (const auto v : polygon.outer())
        points.emplace_back(v.x, v.y, 0);
    for (int i = 0; i < polygon.inners().size(); i++) {
        auto contour = polygon.inners().at(i);
        for (const auto v : contour)
            points.emplace_back(v.x, v.y, 0);
    }

    geometry::mesh result = geometry::detail::buildMesh(points);
    geometry::detail::split_mesh(result, polygon);
    for (int i = 0; i < result.r_triangles().size(); i++) {
        if (!geometry::belongs(geometry::detail::center(result.r_triangles()[i]), polygon)) {
            result.r_triangles()[i] = result.r_triangles().back();
            result.r_triangles().pop_back();
            i = i - 1;
        }
    }
    return result;
}

std::vector<geometry::polygon> geometry::intersect(const geometry::polygon &clip, const geometry::polygon &polygon) {
    gpc_polygon result;
    gpc_polygon p1 = gpc::to_gpc(clip);
    gpc_polygon p2 = gpc::to_gpc(polygon);

    gpc_polygon_clip(GPC_INT, &p1, &p2, &result);
    return gpc::split_gpc(result);
}

std::vector<geometry::polygon> geometry::difference(const geometry::polygon &clip, const geometry::polygon &polygon) {
    gpc_polygon result;
    gpc_polygon p1 = gpc::to_gpc(clip);
    gpc_polygon p2 = gpc::to_gpc(polygon);

    gpc_polygon_clip(GPC_DIFF, &p1, &p2, &result);
    return gpc::split_gpc(result);
}

std::vector<geometry::polygon> geometry::unite(const geometry::polygon &clip, const geometry::polygon &polygon) {
    gpc_polygon result;
    gpc_polygon p1 = gpc::to_gpc(clip);
    gpc_polygon p2 = gpc::to_gpc(polygon);

    gpc_polygon_clip(GPC_UNION, &p1, &p2, &result);
    return gpc::split_gpc(result);
}
