//
// Created by gdhsnlvr on 11.03.17.
//

#ifndef GEOMETRY_LIBRARY_POLYGON_DETAIL_H
#define GEOMETRY_LIBRARY_POLYGON_DETAIL_H


#include "../vec2.h"
#include "../polygon.h"
#include "../mesh.h"

namespace geometry
{
    namespace gpc {
        static gpc_polygon to_gpc(const geometry::polygon &polygon);
        static void gpc_add_contour(gpc_polygon &polygon, const geometry::polygon::contour_t &contour, bool hole);
        static geometry::polygon from_gpc(const gpc_polygon &polygon);
        static std::vector<geometry::polygon> split_gpc(const gpc_polygon &polygon);
    }

    bool belongs(const geometry::vec2 &vec, const geometry::polygon::contour_t &contour);
    bool belongs(const geometry::vec2 &vec, const geometry::polygon &polygon);
    geometry::mesh build_mesh(const geometry::polygon &polygon);

    std::vector<geometry::polygon> intersect(const geometry::polygon &clip, const geometry::polygon &polygon);
    std::vector<geometry::polygon> difference(const geometry::polygon &clip, const geometry::polygon &polygon);
    std::vector<geometry::polygon> unite(const geometry::polygon &clip, const geometry::polygon &polygon);
}

#endif //GEOMETRY_LIBRARY_POLYGON_DETAIL_H