//
// Created by gdhsnlvr on 22.01.17.
//

#ifndef GEOMETRY_LIBRARY_FIND_CONVEX_HULL_H
#define GEOMETRY_LIBRARY_FIND_CONVEX_HULL_H

#include <vector>

#include "../vec2.h"

namespace geometry {

std::vector<vec2> find_convex_hull(const std::vector<vec2> &points);

}


#endif //GEOMETRY_LIBRARY_FIND_CONVEX_HULL_H
