//
// Created by gdhsnlvr on 05.02.17.
//

#ifndef GEOMETRY_LIBRARY_MESH_SMOOTHING_H
#define GEOMETRY_LIBRARY_MESH_SMOOTHING_H


#include "../mesh.h"

namespace geometry {

geometry::mesh smooth_mesh(const geometry::mesh &mesh);

}


#endif //GEOMETRY_LIBRARY_MESH_SMOOTHING_H
