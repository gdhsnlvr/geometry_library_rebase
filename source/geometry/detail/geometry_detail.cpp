//
// Created by gdhsnlvr on 18.09.16.
//

#include <cmath>
#include <iostream>
#include <map>

#include "geometry_detail.h"
#include "mesh_smoothing.h"
#include "polygon_detail.h"

real_t geometry::detail::distance(const geometry::vec2 &v1, const geometry::vec2 &v2)
{
    return geometry::vec2::length(v2 - v1);
}

real_t geometry::detail::distance(const geometry::vec2 &v, const geometry::segment &segment)
{
    std::pair<real_t, real_t> parametersPack;
    geometry::line<geometry::vec2> l1(v, v + geometry::vec2::normal(segment.begin(), segment.end()));
    geometry::line<geometry::vec2> l2(segment.begin(), segment.end());
    if (geometry::detail::intersect(l1, l2, parametersPack)) {
        if (parametersPack.second < 0) {
            return geometry::detail::distance(v, segment.begin());
        }
        else if (parametersPack.second > 1) {
            return geometry::detail::distance(v, segment.end());
        }
        else {
            return geometry::detail::distance(v, segment.begin() + (segment.end() - segment.begin()) * parametersPack.second);
        }
    }
    return 0;
}

real_t geometry::detail::heightAt(const geometry::vec2 &position, const geometry::plane &plane)
{
    if (!geometry::real::fizzyEqual(plane.normal().z, 0)) {
        return (-plane.constant() - plane.normal().x * position.x - plane.normal().y * position.y) / plane.normal().z;
    }
    return 0;
}

real_t geometry::detail::heightAt(const geometry::vec2 &position, const geometry::triangle &triangle)
{
    geometry::vec3 normal = geometry::detail::normal(triangle);
    if (!geometry::real::fizzyEqual(normal.z, 0)) {
        return (geometry::vec3::dot(normal, triangle[0].position) - normal.x * position.x - normal.y * position.y) / normal.z;
    }
    return 0;
}

real_t geometry::detail::heightAt(const geometry::vec2 &position, const geometry::mesh &mesh)
{
    for (const geometry::triangle &triangle : mesh.triangles()) {
        geometry::triangle projection = triangle;
        for (geometry::vertex &v : projection) v.position.z = 0;
        if (belongs(geometry::vec3(position, 0), projection))
            return heightAt(position, triangle);
    }
    return geometry::real::neg_infinity;
}

real_t geometry::detail::heightAtSubSet(const geometry::vec2 &position, const geometry::mesh &mesh,
    const std::vector<int> &subset)
{
    for (int index : subset) {
        geometry::triangle projection = mesh.triangles()[index];
        for (geometry::vertex &v : projection) v.position.z = 0;
        if (belongs(geometry::vec3(position, 0), projection))
            return heightAt(position, mesh.triangles()[index]);
    }
    return geometry::real::neg_infinity;
}

real_t geometry::detail::subs_in_line(const geometry::line<geometry::vec2> &line, const geometry::vec2 &vec)
{
    return geometry::vec2::dot(geometry::detail::normal(line), vec) + line.constant();
}

real_t geometry::detail::distance(const geometry::vec3 &v1, const geometry::vec3 &v2)
{
    return geometry::vec3::length(v2 - v1);
}

real_t geometry::detail::square(const geometry::triangle &triangle)
{
    return geometry::vec3::length(
        geometry::vec3::cross(triangle[1].position - triangle[0].position,
            triangle[2].position - triangle[1].position)
    ) / 2.0;
}

geometry::vec2 geometry::detail::normal(const geometry::line<geometry::vec2> &line)
{
    return geometry::vec2::normal(line.begin(), line.end());
}

geometry::vec2 geometry::detail::normal(const geometry::segment &segment)
{
    return geometry::vec2::normal(segment.begin(), segment.end());
}

geometry::vec3 geometry::detail::normal(const geometry::plane &plane)
{
    return plane.normal();
}

geometry::vec3 geometry::detail::normal(const geometry::triangle &triangle)
{
    return geometry::vec3::normal(triangle[0].position, triangle[1].position, triangle[2].position);
}

bool geometry::detail::intersect(const geometry::segment &s1, const geometry::segment &s2,
    std::pair<real_t, real_t> &parameterPack)
{
    geometry::vec2 v1 = s1.end() - s1.begin();
    geometry::vec2 v2 = s2.begin() - s2.end();
    geometry::vec2 v3 = s2.begin() - s1.begin();

    real_t det = geometry::vec2::cross(v1, v2);
    real_t dt = geometry::vec2::cross(v3, v2);
    real_t ds = geometry::vec2::cross(v1, v3);

    if (geometry::real::abs(det) < geometry::real::epsilon) {
        return false;
    }

    real_t t = dt / det;
    real_t s = ds / det;

    if (geometry::segment::correctParamter(s1, t) && geometry::segment::correctParamter(s2, s)) {
        parameterPack = std::make_pair(t, s);
        return true;
    }

    return false;
}

bool geometry::detail::intersect(const geometry::line<geometry::vec2> &l1, const geometry::line<geometry::vec2> &l2,
    std::pair<real_t, real_t> &parameterPack)
{
    geometry::vec2 v1 = l1.end() - l1.begin();
    geometry::vec2 v2 = l2.begin() - l2.end();
    geometry::vec2 v3 = l2.begin() - l1.begin();

    real_t det = geometry::vec2::cross(v1, v2);
    real_t dt = geometry::vec2::cross(v3, v2);
    real_t ds = geometry::vec2::cross(v1, v3);

    if (geometry::real::abs(det) < geometry::real::epsilon) {
        return false;
    }

    parameterPack.first = dt / det;
    parameterPack.second = ds / det;

    return true;
}

bool geometry::detail::intersect(const geometry::line<geometry::vec2> &l1, const geometry::line<geometry::vec2> &l2,
    geometry::vec2 &result)
{
    std::pair<real_t, real_t> parameterPack;
    if (geometry::detail::intersect(l1, l2, parameterPack)) {
        result = l1.begin() + (l1.end() - l1.begin()) * parameterPack.first;
        return true;
    }
    return false;
}

bool geometry::detail::intersect(const geometry::segment &s1, const geometry::segment &s2, geometry::vec2 &result)
{
    std::pair<real_t, real_t> parameterPack;
    if (geometry::detail::intersect(s1, s2, parameterPack)) {
        result = s1.begin() + (s1.end() - s1.begin()) * parameterPack.first;
        return true;
    }
    return false;
}

bool geometry::detail::belongs(const geometry::vec3 &v, const geometry::plane &plane)
{
    return geometry::real::abs(geometry::vec3::dot(plane.normal(), v) + plane.constant()) < geometry::real::epsilon;
}

bool geometry::detail::belongs(const geometry::vec3 &v, const geometry::triangle &triangle)
{
    if (geometry::vec3::dot(geometry::vec3::cross(triangle[1].position - triangle[0].position, v - triangle[0].position), geometry::detail::normal(triangle)) < 1e-9)
        return false;
    if (geometry::vec3::dot(geometry::vec3::cross(v - triangle[0].position, triangle[2].position - triangle[0].position), geometry::detail::normal(triangle)) < 1e-9)
        return false;
    if (geometry::vec3::dot(geometry::vec3::cross(triangle[1].position - v, triangle[2].position - v), geometry::detail::normal(triangle)) < 1e-9)
        return false;
    return true;
}

geometry::vec2 geometry::detail::center(const geometry::segment &segment)
{
    return (segment.begin() + segment.end()) * 0.5;
}

geometry::vec3 geometry::detail::center(const geometry::triangle &triangle)
{
    geometry::vec3 result;
    for (const geometry::vertex &vertex : triangle)
        result = result + vertex.position;
    return result / 3.0;
}

void geometry::detail::split_mesh(geometry::mesh &mesh, real_t height)
{
    for (int i = 0; i < mesh.r_triangles().size(); i++) {
        geometry::triangle &triangle = mesh.r_triangles()[i];

        for (int j = 0; j < 3; j++) {
            geometry::vertex v1 = triangle[j];
            geometry::vertex v2 = triangle[mod_next(j, 3)];
            geometry::vertex v3 = triangle[mod_next(mod_next(j, 3), 3)];

            if ((v1.position.z > height && v2.position.z < height) ||
                (v1.position.z < height && v2.position.z > height)) {
                if (geometry::real::fizzyEqual(v1.position.z, height) || geometry::real::fizzyEqual(v2.position.z, height)) {
                    continue;
                }

                real_t t = (height - v1.position.z) / (v2.position.z - v1.position.z);
                geometry::vec3 position = v1.position + v2.position * t - v1.position * t;
                geometry::vec3 normal = geometry::vec3::normalized(v1.normal + v2.normal * t - v1.normal * t);
                geometry::vec3 color = v1.color + v2.color * t - v1.color * t;
                position.z = height;
                geometry::vertex vertex(position, normal, color);

                mesh.r_triangles().emplace_back(v2, v3, vertex);
                mesh.r_triangles().emplace_back(vertex, v3, v1);

                mesh.r_triangles()[i] = mesh.r_triangles().back();
                mesh.r_triangles().pop_back();
                i = i - 1;
                break;
            }
        }
    }
}

void geometry::detail::split_mesh(geometry::mesh &mesh, const geometry::segment &segment)
{
    for (int i = 0; i < mesh.r_triangles().size(); i++) {
        geometry::triangle &triangle = mesh.r_triangles()[i];

        for (int j = 0; j < 3; j++) {
            geometry::vec2 p1 = triangle[j].position;
            geometry::vec2 p2 = triangle[mod_next(j, 3)].position;
            if (geometry::detail::distance(p1, p2) < 1e-6) continue;


            geometry::segment edge(p1, p2);
            geometry::vec2 intersection;
            if (!geometry::detail::intersect(segment, edge, intersection)) continue;

            real_t t = geometry::detail::distance(intersection, p1) / geometry::detail::distance(p1, p2);
            if (t < 0.01 || t > 0.99) continue;

            geometry::vertex v1 = triangle[j];
            geometry::vertex v2 = triangle[mod_next(j, 3)];
            geometry::vertex v3 = triangle[mod_next(mod_next(j, 3), 3)];
            geometry::vec3 position = v1.position + v2.position * t - v1.position * t;
            geometry::vec3 normal = geometry::vec3::normalized(v1.normal + v2.normal * t - v1.normal * t);
            geometry::vec3 color = v1.color + v2.color * t - v1.color * t;
            geometry::vertex vertex(position, normal, color);

            mesh.r_triangles().emplace_back(v2, v3, vertex);
            mesh.r_triangles().emplace_back(vertex, v3, v1);

            mesh.r_triangles()[i] = mesh.r_triangles().back();
            mesh.r_triangles().pop_back();
            i = i - 1;
            break;
        }
    }
}

void geometry::detail::split_mesh(geometry::mesh &mesh, const geometry::polygon::contour_t &contour) {
    for (int i = 0; i < contour.size(); i++) {
        int in = (i+1) % (int) contour.size();

        split_mesh(mesh, geometry::segment(contour[i], contour[in]));
    }
}

void geometry::detail::split_mesh(geometry::mesh &mesh, const geometry::polygon &polygon) {
    split_mesh(mesh, polygon.outer());
    for (const auto &inner : polygon.inners())
        split_mesh(mesh, inner);
}

geometry::mesh geometry::detail::slice_mesh(const geometry::mesh &mesh, real_t from, real_t to)
{
    geometry::mesh result = mesh;
    geometry::detail::split_mesh(result, from);
    geometry::detail::split_mesh(result, to);
    for (int i = 0; i < result.r_triangles().size(); i++) {
        if (geometry::detail::center(result.r_triangles()[i]).z > to) {
            for (int j = 0; j < 3; j++) {
                result.r_triangles()[i][j].position.z = to;
                result.r_triangles()[i][j].normal = geometry::vec3(0, 0, 1);
            }
        }
        else if (geometry::detail::center(result.r_triangles()[i]).z < from) {
            result.r_triangles()[i] = result.r_triangles().back();
            result.r_triangles().pop_back();
            i = i - 1;
        }
    }

    for (int i = 0; i < result.r_triangles().size(); i++) {
        if (geometry::detail::square(result.r_triangles()[i]) < geometry::real::epsilon) {
            result.r_triangles()[i] = result.r_triangles().back();
            result.r_triangles().pop_back();
            i = i - 1;
        }
    }

    int oldCountOfTriangles = (int)result.r_triangles().size();
    for (int i = 0; i < oldCountOfTriangles; i++) {
        geometry::triangle triangle = result.r_triangles()[i];
        for (geometry::vertex &vertex : triangle) {
            vertex.position.z = from;
            vertex.normal = geometry::vec3(0, 0, -1);
        }
        result.r_triangles().push_back(triangle);
    }

    result.updateBoundary();
    return result;
}

std::vector<int> geometry::detail::buildMeshIndexes(const std::vector<geometry::vec3> &initialPoints)
{
    std::vector<geometry::vec2> projectedPoints;
    std::transform(initialPoints.begin(), initialPoints.end(), std::back_inserter(projectedPoints),
        [](const geometry::vec3 &v) { return geometry::vec2(v); }
    );

    geometry::delaunay_triangulation triangulation(projectedPoints);
    triangulation.build();
    return triangulation.result();
}

geometry::mesh geometry::detail::buildMesh(const std::vector<geometry::vec3> &initialPoints,
    const std::vector<int> &indexes)
{
    std::map<int, geometry::vec3> normals;
    for (int i = 0; i < indexes.size(); i += 3) {
        geometry::vec3 normal = geometry::vec3::normal(
            initialPoints[indexes[i]], initialPoints[indexes[i + 1]], initialPoints[indexes[i + 2]]
        );

        real_t c = -geometry::vec3::dot(normal, initialPoints[indexes[i]]);
        if (geometry::vec3::dot(normal, geometry::vec3(0, 0, geometry::real::infinity)) + c < 0) {
            normal = normal * (-1.0);
        }

        for (int j = 0; j < 3; j++) {
            normals[i + j] = normals[i + j] + normal;
        }
    }

    std::vector<geometry::triangle> triangles;
    for (int i = 0; i < indexes.size(); i += 3) {
        geometry::vertex v1(initialPoints[indexes[i]], geometry::vec3::normalized(normals[i]), geometry::vec3(1, 1, 1));
        geometry::vertex v2(initialPoints[indexes[i + 1]], geometry::vec3::normalized(normals[i + 1]), geometry::vec3(1, 1, 1));
        geometry::vertex v3(initialPoints[indexes[i + 2]], geometry::vec3::normalized(normals[i + 2]), geometry::vec3(1, 1, 1));
        triangles.emplace_back(v1, v2, v3);
    }

    return geometry::mesh(triangles);
}

geometry::mesh geometry::detail::buildMesh(const std::vector<geometry::vec3> &initialPoints) {
    std::vector<geometry::vec3> cloud;
    for (int i = 0; i < initialPoints.size(); i++) {
        bool ok = initialPoints[i].z > 0;
        for (int j = 0; j < initialPoints.size(); j++) {
            if (i != j && geometry::vec3::length(initialPoints[i] - initialPoints[j]) < 0.01) {
                ok = false;
            }
        }

        if (ok) {
            cloud.push_back(initialPoints[i]);
        }
    }

    auto indexes = buildMeshIndexes(cloud);
    return smooth_mesh(buildMesh(cloud, indexes));
}

geometry::mesh geometry::detail::normalizeMesh(const geometry::mesh &mesh, const geometry::vec3 &sizes) {
    geometry::mesh result = mesh;
    geometry::vec3 oldSize = mesh.maxBoundary() - mesh.minBoundary();
    if (mesh.triangles().empty()) {
        return result;
    }

    real_t maxSize = geometry::real::max(geometry::real::max(oldSize.x, oldSize.y), oldSize.z);
    for (geometry::triangle &triangle : result.r_triangles()) {
        for (geometry::vertex &vertex : triangle) {
            vertex.position = vertex.position - mesh.minBoundary();
            vertex.position.z = vertex.position.z;
        }
    }

    result.updateBoundary();

    for (geometry::triangle &triangle : result.r_triangles()) {
        for (geometry::vertex &vertex : triangle) {
            vertex.position = vertex.position - result.maxBoundary() / 2.0;
        }
    }

    return result;
}

real_t geometry::detail::rayCasting(const geometry::plane &plane, const geometry::line<geometry::vec3> &ray)
{
    if (geometry::real::abs(geometry::vec3::dot(geometry::detail::normal(plane), ray.end() - ray.begin())) > geometry::real::epsilon) {
        real_t t = -(geometry::vec3::dot(plane.normal(), ray.begin()) + plane.constant()) /
            geometry::vec3::dot(ray.end() - ray.begin(), plane.normal());
        return t;
    }
    return geometry::real::neg_infinity;
}

real_t geometry::detail::rayCasting(const geometry::triangle &triangle, const geometry::line<geometry::vec3> &ray)
{
    real_t t = geometry::detail::rayCasting(geometry::plane(triangle[0].position, triangle[1].position, triangle[2].position), ray);
    if (t > 0 && geometry::detail::belongs(ray.begin() + (ray.end() - ray.begin()) * t, triangle))
        return t;
    return geometry::real::neg_infinity;
}

real_t geometry::detail::rayCasting(const geometry::mesh &mesh, const geometry::line<geometry::vec3> &ray)
{
    real_t t = geometry::real::neg_infinity;
    for (const geometry::triangle &triangle : mesh.triangles()) {
        t = geometry::real::max(t, geometry::detail::rayCasting(triangle, ray));
    }
    return t;
}

geometry::mapped_mesh geometry::detail::mapMesh(const geometry::mesh &mesh, const geometry::vec2 &size)
{
    geometry::mapped_mesh mapped_mesh;

    auto containingRaster = [](real_t x, real_t size) {
        return (std::size_t) std::floor(x / size);
    };

    auto getRasterPosition = [](std::size_t i, std::size_t j, const geometry::vec2 &size) {
        return geometry::vec2(i * size.x, j * size.y);
    };

    geometry::vec3 square = mesh.maxBoundary() - mesh.minBoundary();
    std::size_t countX = containingRaster(square.x, size.x) + 1;
    std::size_t countY = containingRaster(square.y, size.y) + 1;
    mapped_mesh.r_map().assign(countX, std::vector<std::vector<int> >(countY));

    for (int i = 0; i < mesh.triangles().size(); i++) {
        geometry::vec2 v1 = mesh.triangles()[i][0].position;
        geometry::vec2 v2 = mesh.triangles()[i][1].position;
        geometry::vec2 v3 = mesh.triangles()[i][2].position;
        geometry::vec2 maxV = geometry::vec2::componentMax(v1, geometry::vec2::componentMax(v2, v3));
        geometry::vec2 minV = geometry::vec2::componentMin(v1, geometry::vec2::componentMin(v2, v3));
        std::size_t fromX = containingRaster(minV.x - mesh.minBoundary().x, size.x);
        std::size_t toX = containingRaster(maxV.x - mesh.minBoundary().x, size.x);
        std::size_t fromY = containingRaster(minV.y - mesh.minBoundary().y, size.y);
        std::size_t toY = containingRaster(maxV.y - mesh.minBoundary().y, size.y);
        for (std::size_t x = fromX; x <= toX; x++) {
            for (std::size_t y = fromY; y <= toY; y++) {
                mapped_mesh.r_map()[x][y].push_back(i);
            }
        }
    }

    return mapped_mesh;
}

geometry::horizon_rastarization geometry::detail::rastarizate(const Horizon &horizon, const geometry::vec2 &size, int detalization)
{
    geometry::horizon_rastarization horizon_rastarization;
    geometry::mapped_mesh mapped_mesh = geometry::detail::mapMesh(horizon.mesh(), size / detalization);

    auto containingRaster = [](real_t x, real_t size) {
        return (std::size_t) std::floor(x / size);
    };

    auto getRasterPosition = [](std::size_t i, std::size_t j, const geometry::vec2 &size) {
        return geometry::vec2(i * size.x, j * size.y);
    };

    geometry::vec3 square = horizon.mesh().maxBoundary() - horizon.mesh().minBoundary();
    std::size_t countX = containingRaster(square.x, size.x) + 1;
    std::size_t countY = containingRaster(square.y, size.y) + 1;

    geometry::vec2 gridBegin = horizon.mesh().minBoundary();

    geometry::vec2 detalizationSize = size / detalization;
    real_t maxHeight = horizon.heightMax() - horizon.heightMin();

    horizon_rastarization.r_rasters().assign(countX, geometry::horizon_rastarization::row_t(countY));
    for (std::size_t i = 0; i < countX; i++) {
        for (std::size_t j = 0; j < countY; j++) {
            geometry::vec2 rasterPosition = gridBegin + getRasterPosition(i, j, size);
            real_t localHeight = 0;
            for (std::size_t id = 0; id <= detalization; id++) {
                for (std::size_t jd = 0; jd <= detalization; jd++) {
                    geometry::vec2 point = rasterPosition + getRasterPosition(id, jd, detalizationSize);
                    if (point.x > horizon.mesh().maxBoundary().x || point.x < horizon.mesh().minBoundary().x ||
                        point.y > horizon.mesh().maxBoundary().y || point.y < horizon.mesh().minBoundary().y) {
                        continue;
                    }

                    geometry::vec2 offset = point - horizon.mesh().minBoundary();
                    std::size_t mappedX = containingRaster(offset.x, detalizationSize.x);
                    std::size_t mappedY = containingRaster(offset.y, detalizationSize.y);
                    real_t height = geometry::detail::heightAtSubSet(
                        point, horizon.mesh(), mapped_mesh.map()[mappedX][mappedY]
                    ) - horizon.heightMin();
                    if (height < 0) continue;

                    if (id == 0 || jd == 0 || id == detalization || jd == detalization) {
                        if (id == 0 && jd == 0) {
                            localHeight += height;
                        }
                        else if (id == 0 && jd == detalization) {
                            localHeight += height;
                        }
                        else if (id == detalization && jd == detalization) {
                            localHeight += height;
                        }
                        else if (id == detalization && jd == 0) {
                            localHeight += height;
                        }
                        else {
                            localHeight += height * 2.0;
                        }
                    }
                    else {
                        localHeight += height * 4.0;
                    }
                }
            }
            localHeight *= detalizationSize.x * detalizationSize.y / 4.0;
            horizon_rastarization.r_rasters()[i][j].setStoneVolume(localHeight / (maxHeight * size.x * size.y));
        }
    }

    return horizon_rastarization;
}

std::vector<geometry::vec2>
geometry::detail::sort_by_angle(const std::vector<geometry::vec2> &points)
{
    std::vector<geometry::vec2> buffer(points);

    geometry::vec2 base = *std::min_element(buffer.begin(), buffer.end(),
                                            [] (const geometry::vec2 &p1, const geometry::vec2 &p2) {
                                                return p1.y < p2.y || (p1.y == p2.y && p1.x < p2.x);
                                            }
    );

    std::sort(buffer.begin(), buffer.end(),
              [base] (const geometry::vec2 &p1, const geometry::vec2 &p2) {
                  if (geometry::vec2::cross(p1 - base, p2 - base) == 0)
                      return geometry::vec2::length(p1 - base) < geometry::vec2::length(p2 - base);
                  return geometry::vec2::cross(p1 - base, p2 - base) > 0;
              }
    );

    return buffer;
}

std::vector<int>
geometry::detail::sort_by_angle(const std::vector<int> &indexes,
                                const std::vector<geometry::vec2> &points)
{
    std::vector<int> buffer(indexes);

    int base = *std::min_element(
            buffer.begin(), buffer.end(),
            [&points] (int p1, int p2) {
                return points[p1].y < points[p2].y ||
                       (points[p1].y == points[p2].y && points[p1].x < points[p2].x);
            }
    );

    std::sort(buffer.begin(), buffer.end(),
              [base, &points] (int p1, int p2) {
                  if (geometry::vec2::cross(points[p1] - points[base], points[p2] - points[base]) == 0)
                      return geometry::vec2::length(points[p1] - points[base]) < geometry::vec2::length(points[p2] - points[base]);
                  return geometry::vec2::cross(points[p1] - points[base], points[p2] - points[base]) > 0;
              }
    );

    return buffer;
}

geometry::mesh geometry::detail::cut_mesh(const geometry::mesh &mesh, const geometry::polygon &polygon) {
    geometry::mesh result = mesh;
    geometry::detail::split_mesh(result, polygon);
    for (int i = 0; i < result.triangles().size(); i++) {
        auto &trinagle = result.triangles()[i];
        geometry::vec2 center = geometry::detail::center(trinagle);
        if (!geometry::belongs(center, polygon)) {
            result.r_triangles()[i] = result.triangles().back();
            result.r_triangles().pop_back();
        }
    }
    return result;
}
