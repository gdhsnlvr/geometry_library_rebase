//
// Created by gdhsnlvr on 27.05.17.
//

#ifndef GEOMETRY_LIBRARY_MESH_CROSS_H
#define GEOMETRY_LIBRARY_MESH_CROSS_H


#include <vector>
#include "../mesh.h"

namespace geometry
{

std::vector<geometry::vec2> get_heights_on_segment(const geometry::mesh &mesh,
                                           const geometry::segment &segment);

std::vector<geometry::vec2> get_heights_on_cross(const geometry::mesh &mesh,
                                         const std::vector<geometry::vec2> &points);

}


#endif //GEOMETRY_LIBRARY_MESH_CROSS_H
