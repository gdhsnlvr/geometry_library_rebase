//
// Created by gdhsnlvr on 07.02.17.
//

#ifndef GEOMETRY_LIBRARY_QUAD_MESH_H
#define GEOMETRY_LIBRARY_QUAD_MESH_H

#include <vector>
#include "vec3.h"
#include "quad.h"

namespace geometry {
    class quad_mesh
    {
    protected:
        std::vector<geometry::quad> m_quads;
        geometry::vec3 m_minBoundary;
        geometry::vec3 m_maxBoundary;

    public:
        quad_mesh() = default;
        quad_mesh(const std::vector<geometry::quad> quads);

        const std::vector<geometry::quad> &quads() const;
        std::vector<geometry::quad> &r_quads();

        const geometry::vec3 &minBoundary() const;
        const geometry::vec3 &maxBoundary() const;

        void updateBoundary();
    };
}


#endif //GEOMETRY_LIBRARY_QUAD_MESH_H
