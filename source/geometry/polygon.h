//
// Created by gdhsnlvr on 24.09.16.
//

#ifndef GEOMETRY_LIBRARY_POLYGON_H
#define GEOMETRY_LIBRARY_POLYGON_H

#include <vector>

#include "vec2.h"
#include "gpc/gpc.h"

namespace geometry
{
    class polygon
    {
    public:
        using contour_t = std::vector<geometry::vec2>;
    protected:
        contour_t m_outer;
        std::vector<contour_t> m_inners;

    public:
        polygon() = default;
        polygon(const contour_t &contour);
        polygon(const contour_t &outer, const std::vector<contour_t> &inners);

        const contour_t &outer() const;
        const std::vector<contour_t> &inners() const;
        void set_outer(const contour_t &contour);
        void add_inner(const contour_t &contour);
    };
}


#endif //GEOMETRY_LIBRARY_POLYGON_H