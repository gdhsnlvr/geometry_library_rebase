//
// Created by gdhsnlvr on 19.01.17.
//

#include <algorithm>
#include <cassert>

#include "voronoi_diagram.h"
#include "detail/geometry_detail.h"

namespace geometry {

int voronoi_diagram::split_edge_with_reversed_by_point(voronoi_edge *edge, vec2 point)
{
    int point_index = (int) m_points.size();
    voronoi_edge *reversed = edge->reversed;

    voronoi_edge *new_edge = new voronoi_edge();
    new_edge->from = edge->from;
    new_edge->to = point_index;
    new_edge->owner = edge->owner;
    new_edge->reversed = reversed;
    reversed->reversed = new_edge;
    edge->from = point_index;

    voronoi_edge *new_edge_reversed = new voronoi_edge();
    new_edge_reversed->from = reversed->from;
    new_edge_reversed->to = point_index;
    new_edge_reversed->owner = reversed->owner;
    new_edge_reversed->reversed = edge;
    edge->reversed = new_edge_reversed;
    reversed->from = point_index;

    m_points.push_back(point);
    return point_index;
}

voronoi_locus voronoi_diagram::find_any_infinity_locus() const
{
    assert(m_locuses.size() > 0);

    for (voronoi_locus locus : m_locuses) {
        voronoi_edge *head = locus.list_head;
        do {
            if (head->type != geometry::finite)
                return locus;
            head = head->next;
        } while(head != locus.list_head);
    }

    return m_locuses.front();
}

std::vector<int> voronoi_diagram::find_convex_hull() const
{
    assert(m_locuses.size() > 0);

    std::vector<int> convex_hull;
    for (voronoi_locus locus : m_locuses) {
        voronoi_edge *head = locus.list_head;

        do {
            if (head->type != geometry::finite) {
                convex_hull.push_back(locus.index);
                break;
            }

            head = head->next;
        } while (head != locus.list_head);
    }

    return geometry::detail::sort_by_angle(convex_hull, m_cites);
}

voronoi_diagram::~voronoi_diagram()
{

}

}