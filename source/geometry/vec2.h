//
// Created by gdhsnlvr on 18.09.16.
//

#ifndef GEOMETRY_LIBRARY_VEC2_H
#define GEOMETRY_LIBRARY_VEC2_H

#include "vec3.h"

namespace geometry {
    using vec2 = vec3::vec2;
}

#endif //GEOMETRY_LIBRARY_VEC2_H
