//
// Created by gdhsnlvr on 07.02.17.
//

#include "quad.h"

geometry::quad::quad(const geometry::vertex &v1,
                     const geometry::vertex &v2,
                     const geometry::vertex &v3,
                     const geometry::vertex &v4)
{
    m_vertexes[0] = v1;
    m_vertexes[1] = v2;
    m_vertexes[2] = v3;
    m_vertexes[3] = v4;
}

const geometry::vertex &geometry::quad::operator[](std::size_t index) const
{
    return m_vertexes[index];
}

geometry::vertex &geometry::quad::operator[](std::size_t index)
{
    return m_vertexes[index];
}

std::array<geometry::vertex, 4>::const_iterator geometry::quad::begin() const
{
    return m_vertexes.begin();
}

std::array<geometry::vertex, 4>::const_iterator geometry::quad::end() const
{
    return m_vertexes.end();
}

std::array<geometry::vertex, 4>::iterator geometry::quad::begin()
{
    return m_vertexes.begin();
}

std::array<geometry::vertex, 4>::iterator geometry::quad::end()
{
    return m_vertexes.end();
}
