//
// Created by gdh on 24.07.16.
//

#ifndef GEOMETRY_LIBRARY_DEFENITIONS_HPP
#define GEOMETRY_LIBRARY_DEFENITIONS_HPP

#include <cmath>

using real_t = double;
namespace geometry
{
    class real
    {
    public:
        static constexpr real_t infinity = 1e18;
        static constexpr real_t neg_infinity = -1e18;
        static constexpr real_t epsilon = 1e-6;

        static bool fizzyEqual(real_t a, real_t b);
        static bool fizzyLess(real_t a, real_t b);
        static bool fizzyGreater(real_t a, real_t b);

        static real_t abs(real_t x);
        static real_t max(real_t a, real_t b);
        static real_t min(real_t a, real_t b);
    };
}

#define mod_next(i, n) (i + 1) * (i != n - 1)

#endif //GEOMETRY_LIBRARY_DEFENITIONS_HPP
