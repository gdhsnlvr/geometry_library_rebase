//
// Created by gdhsnlvr on 18.09.16.
//

#include "segment.h"

geometry::segment::segment(const geometry::vec2 &begin, const geometry::vec2 &end, geometry::segment::type _type)
        : m_begin(begin), m_end(end), m_type(_type)
{
}

bool geometry::segment::correctParamter(const geometry::segment &s, real_t parameter)
{
    return s.m_type == geometry::segment::type::Segment   && 0 < parameter + geometry::real::epsilon
           && parameter < 1 + geometry::real::epsilon ||
           s.m_type == geometry::segment::type::Interval  && geometry::real::epsilon < parameter
           && parameter + geometry::real::epsilon < 1 ||
           s.m_type == geometry::segment::type::RightOpenInterval && 0 < parameter + geometry::real::epsilon
           && parameter + geometry::real::epsilon < 1 ||
           s.m_type == geometry::segment::type::LeftOpenInterval  && geometry::real::epsilon < parameter
           && parameter < 1 + geometry::real::epsilon;
}

const geometry::vec2 &geometry::segment::begin() const {
    return m_begin;
}

const geometry::vec2 &geometry::segment::end() const {
    return m_end;
}
