#ifndef GEOMETRY_LIBRARY_DELAUNAYTRIANGULATION_H
#define GEOMETRY_LIBRARY_DELAUNAYTRIANGULATION_H

#include <vector>
#include <array>
#include <map>
#include <algorithm>
#include <cmath>

#include "vec2.h"

namespace geometry
{
    class delaunay_triangulation
    {
    protected:
        using delaunayTriangle = std::array<int, 3>;
        using delaunayEdge = std::array<int, 2>;

        std::vector<geometry::vec2> m_buffer;
        std::vector<int> m_triangles;
        delaunayTriangle m_superTriangle;
        int m_count;

    protected:
        delaunayTriangle make_triangle(int i1, int i2, int i3) const;
        delaunayEdge make_edge(int i1, int i2) const;

        void insert(int index);
        bool insideCircumcircle(int i1, int i2, int i3, int p) const;

    public:
        delaunay_triangulation() = default;
        delaunay_triangulation(const std::vector<geometry::vec2> &initialPoints);

        void build();
        std::vector<int> result() const;
    };
}

#endif //GEOMETRY_LIBRARY_DELAUNAYTRIANGULATION_H

