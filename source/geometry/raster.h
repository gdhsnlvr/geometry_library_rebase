//
// Created by gdhsnlvr on 03.10.16.
//

#ifndef GEOMETRY_LIBRARY_RASTER_H
#define GEOMETRY_LIBRARY_RASTER_H

#include "real.h"

namespace geometry
{
    class raster
    {
    protected:
        real_t m_stoneVolume;

    public:
        raster();

        real_t stoneVolume() const;
        void setStoneVolume(real_t volume);
    };
}


#endif //GEOMETRY_LIBRARY_RASTER_H
