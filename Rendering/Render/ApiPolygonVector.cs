using System;
using System.Runtime.InteropServices;

namespace Render
{
    public class ApiPolygonVector
    {
        #region Import
        
        private const string DllFilePath = @"Geology.Geometry.dll";

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_vector_new(IntPtr[] polygons, int n);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_vector_get(IntPtr obj, int i);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_polygon_vector_set(IntPtr obj, int i, IntPtr v);

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int api_polygon_vector_size(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_polygon_vector_free(IntPtr obj);
        
        #endregion

        public readonly IntPtr Self;
        public int Count() => api_polygon_vector_size(Self);

        public ApiPolygonVector(Polygon[] polygons)
        {
            var intPtrs = new IntPtr[polygons.Length];
            for (var i = 0; i < polygons.Length; i++)
            {
                intPtrs[i] = polygons[i].Self;
            }
            
            Self = api_polygon_vector_new(intPtrs, intPtrs.Length);
        }

        public ApiPolygonVector(IntPtr self)
        {
            Self = self;
        }

        ~ApiPolygonVector()
        {
            api_polygon_vector_free(Self);
        }
        
        public Polygon[] ToArray()
        {
            var data = new Polygon[Count()];
            for (var i = 0; i < Count(); i++)
            {
                data[i] = this[i];
            }
            return data;
        }
        
        public Polygon this[int key]
        {
            get => new Polygon(api_polygon_vector_get(Self, key));
            set => api_polygon_vector_set(Self, key, value.Self);
        }
    }
}