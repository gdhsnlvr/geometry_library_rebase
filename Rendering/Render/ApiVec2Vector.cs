using System;
using System.Runtime.InteropServices;

namespace Render
{
    public class ApiVec2Vector
    {
        #region Import
        
        private const string DllFilePath = @"Geology.Geometry.dll";

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_vec2_vector_new(IntPtr[] points, int n);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_vec2_vector_get(IntPtr obj, int i);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_vec2_vector_set(IntPtr obj, int i, IntPtr v);

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int api_vec2_vector_size(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_vec2_vector_free(IntPtr obj);
        
        #endregion

        public readonly IntPtr Self;
        public int Count() => api_vec2_vector_size(Self);

        public ApiVec2Vector(Vec2[] points)
        {
            var intPtrs = new IntPtr[points.Length];
            for (var i = 0; i < points.Length; i++)
            {
                intPtrs[i] = points[i].Self;
            }
            
            Self = api_vec2_vector_new(intPtrs, intPtrs.Length);
        }

        public ApiVec2Vector(IntPtr self)
        {
            Self = self;
        }

        ~ApiVec2Vector()
        {
            api_vec2_vector_free(Self);
        }
        
        public Vec2[] ToArray()
        {
            var data = new Vec2[Count()];
            for (var i = 0; i < Count(); i++)
            {
                data[i] = this[i];
            }
            return data;
        }
        
        public Vec2 this[int key]
        {
            get => new Vec2(api_vec2_vector_get(Self, key));
            set => api_vec2_vector_set(Self, key, value.Self);
        }
    }
}