using System;
using System.Runtime.InteropServices;

namespace Render
{
    public class Vec2
    {
        #region Import
        
        private const string DllFilePath = @"Geology.Geometry.dll";

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_vec2_new(double x, double y);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern double api_vec2_x(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern double api_vec2_y(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_vec2_free(IntPtr obj);
        
        #endregion
        
        public readonly IntPtr Self;
        public double X => api_vec2_x(Self);
        public double Y => api_vec2_x(Self);

        public Vec2(double x, double y)
        {
            Self = api_vec2_new(x, y);
        }

        public Vec2(IntPtr self)
        {
            Self = self;
        }
        
        ~Vec2() {
            api_vec2_free(Self);
        }
    }
}