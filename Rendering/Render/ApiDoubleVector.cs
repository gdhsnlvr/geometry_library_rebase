using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace Render
{
    public class ApiDoubleVector
    {
        #region Import
        
        private const string DllFilePath = @"Geology.Geometry.dll";

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_double_vector_new(double[] data, int size);

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern double api_double_vector_get(IntPtr obj, int index);

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_double_vector_set(IntPtr obj, int index, double value);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int api_double_vector_size(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_double_vector_free(IntPtr obj);
        
        #endregion
        
        private readonly IntPtr _self;

        public ApiDoubleVector(double[] data)
        {
            _self = api_double_vector_new(data, data.Length);
        }

        public ApiDoubleVector(IntPtr self)
        {
            _self = self;
        }

        public double this[int key]
        {
            get => api_double_vector_get(_self, key);
            set => api_double_vector_set(_self, key, value);
        }

        public int Count() => api_double_vector_size(_self);

        public double[] ToArray()
        {
            var data = new double[Count()];
            for (var i = 0; i < Count(); i++)
            {
                data[i] = this[i];
            }
            return data;
        }

        ~ApiDoubleVector()
        {
            api_double_vector_free(_self);
        }
    }
}