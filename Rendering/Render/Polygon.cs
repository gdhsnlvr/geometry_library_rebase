using System;
using System.Runtime.InteropServices;

namespace Render
{
    public class Polygon
    {
        #region Import
        
        private const string DllFilePath = @"Geology.Geometry.dll";

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_new(IntPtr vec2Vector);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_outer(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_inner(IntPtr obj, int index);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int api_polygon_inner_count(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void api_polygon_free(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_unite(IntPtr obj, IntPtr a);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_intersect(IntPtr obj, IntPtr a);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_difference(IntPtr obj, IntPtr a);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_polygon_build_mesh(IntPtr obj);
        
        #endregion

        public readonly IntPtr Self;
        public Vec2[] Outer => new ApiVec2Vector(api_polygon_outer(Self)).ToArray();
        public Vec2[] this[int key] => new ApiVec2Vector(api_polygon_inner(Self, key)).ToArray();
        public int Count => api_polygon_inner_count(Self);

        public Polygon(ApiVec2Vector vec2Vector)
        {
            Self = api_polygon_new(vec2Vector.Self);
        }

        public Polygon(IntPtr self)
        {
            Self = self;
        }

        ~Polygon()
        {
            api_polygon_free(Self);
        }

        Polygon[] Unite(Polygon polygon)
        {
            return new ApiPolygonVector(api_polygon_unite(Self, polygon.Self)).ToArray();
        }
        
        Polygon[] Intersect(Polygon polygon)
        {
            return new ApiPolygonVector(api_polygon_intersect(Self, polygon.Self)).ToArray();
        }
        
        Polygon[] Difference(Polygon polygon)
        {
            return new ApiPolygonVector(api_polygon_difference(Self, polygon.Self)).ToArray();
        }

        Mesh Mesh()
        {
            return new Mesh(api_polygon_build_mesh(Self));
        }
    }
}