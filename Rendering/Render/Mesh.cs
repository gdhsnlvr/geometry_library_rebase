using System;
using System.Runtime.InteropServices;

namespace Render
{
    public class Mesh
    {
        #region Import
        
        private const string DllFilePath = @"Geology.Geometry.dll";

        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_position_buffer(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_normal_buffer(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_color_buffer(IntPtr obj);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern double api_ray_pick_parameter(IntPtr obj, 
                                                           double px, double py, double pz,
                                                           double ex, double ey, double ez);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_build_surface(double[] x, double[] y, double[] z, int count);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_build_cross(IntPtr obj, double[] x, double[] y, int count);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_build_horizon(IntPtr obj, double from, double to);
        
        [DllImport(DllFilePath, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr api_cut_mesh(IntPtr obj, IntPtr polygon);
        
        #endregion

        public readonly IntPtr Self;
        public double[] PositionBuffer { get; private set; }
        public double[] NormalBuffer { get; private set; }
        public double[] ColorBuffer { get; private set; }

        /// <summary>
        /// Get mesh from memory
        /// </summary>
        /// <param name="self"></param>
        public Mesh(IntPtr self)
        {
            Self = self;
            LoadBuffers();
        }

        public static Mesh BuildSurface(double[] x, double[] y, double[] z)
        {
            return new Mesh(api_build_surface(x, y, z, x.Length));
        }

        public Mesh Slice(double from, double to)
        {
            return new Mesh(api_build_horizon(Self, from, to));
        }

        public Mesh Cross(double[] x, double[] y)
        {
            return new Mesh(api_build_cross(Self, x, y, x.Length));
        }
        
        public double RayPickParameter(Vertex3 position, Vertex3 eye)
        {
            return api_ray_pick_parameter(Self, position.x, position.y, position.z, eye.x, eye.y, eye.z); 
        }

        public Mesh Cut(Polygon polygon)
        {
            return new Mesh(api_cut_mesh(Self, polygon.Self));
        }

        /// <summary>
        /// Load buffers
        /// </summary>
        private void LoadBuffers()
        {
            PositionBuffer = new ApiDoubleVector(api_position_buffer(Self)).ToArray();
            NormalBuffer = new ApiDoubleVector(api_normal_buffer(Self)).ToArray();
            ColorBuffer = new ApiDoubleVector(api_color_buffer(Self)).ToArray();
        }
    }
}