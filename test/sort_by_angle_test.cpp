//
// Created by gdhsnlvr on 22.01.17.
//


#include <gtest/gtest.h>

#include "../source/geometry/vec2.h"
#include "../source/geometry/detail/geometry_detail.h"

TEST(SortByAngle, point_sort) {
    std::vector<geometry::vec2> points;
    points.emplace_back(0, 0);
    points.emplace_back(1, 0);
    points.emplace_back(2, 2);
    points.emplace_back(0, 1);
    points.emplace_back(1, 1);
    points = geometry::detail::sort_by_angle(points);

    std::vector<geometry::vec2> points_true_order;
    points_true_order.emplace_back(0, 0);
    points_true_order.emplace_back(1, 0);
    points_true_order.emplace_back(1, 1);
    points_true_order.emplace_back(2, 2);
    points_true_order.emplace_back(0, 1);

    ASSERT_EQ(points.size(), points_true_order.size());
    for (int i = 0; i < points.size(); i++) {
        ASSERT_DOUBLE_EQ(points[i].x, points_true_order[i].x);
        ASSERT_DOUBLE_EQ(points[i].y, points_true_order[i].y);
    }
}

TEST(SortByAngle, index_sort) {
    std::vector<geometry::vec2> points;
    points.emplace_back(0, 0);
    points.emplace_back(1, 0);
    points.emplace_back(2, 2);
    points.emplace_back(0, 1);
    points.emplace_back(1, 1);
    std::vector<int> indexes {0, 1, 2, 3, 4};
    indexes = geometry::detail::sort_by_angle(indexes, points);

    std::vector<int> indexes_true_order {0, 1, 4, 2, 3};

    ASSERT_EQ(indexes.size(), indexes_true_order.size());
    for (int i = 0; i < indexes.size(); i++) {
        ASSERT_DOUBLE_EQ(indexes[i], indexes_true_order[i]);
    }
}