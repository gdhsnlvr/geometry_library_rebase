//
// Created by gdhsnlvr on 22.01.17.
//


#include <gtest/gtest.h>
#include <random>

#include "../source/geometry/vec2.h"
#include "../source/geometry/detail/unite_convex_hull.h"
#include "../source/geometry/detail/find_convex_hull_test.h"
#include "../source/geometry/detail/geometry_detail.h"

TEST(FindBridgeTest, simple_test) {
    std::vector<geometry::vec2> points;
    // square
    points.emplace_back(0, 0);
    points.emplace_back(1, 0);
    points.emplace_back(1, 1);
    points.emplace_back(0, 1);
    // triangle
    points.emplace_back(2, 0);
    points.emplace_back(4, 0);
    points.emplace_back(3, 1);

    std::vector<int> i_left {0, 1, 2, 3};
    std::vector<int> i_right {4, 5, 6};

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    ASSERT_EQ(bridges.size(), 2);
    ASSERT_EQ(bridges[0].first, 1);
    ASSERT_EQ(bridges[0].second, 4);
    ASSERT_EQ(bridges[1].first, 2);
    ASSERT_EQ(bridges[1].second, 6);
}

TEST(FindBridgeTest, simple_test_2) {
    std::vector<geometry::vec2> points;
    // square
    points.emplace_back(0, 0);
    points.emplace_back(1, 0);
    points.emplace_back(1, 1);
    points.emplace_back(0, 1);
    // triangle
    points.emplace_back(2, 3);
    points.emplace_back(4, 3);
    points.emplace_back(3, 4);

    std::vector<int> i_left {0, 1, 2, 3};
    std::vector<int> i_right {4, 5, 6};

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    ASSERT_EQ(bridges.size(), 2);
    ASSERT_EQ(bridges[0].first, 1);
    ASSERT_EQ(bridges[0].second, 5);
    ASSERT_EQ(bridges[1].first, 3);
    ASSERT_EQ(bridges[1].second, 4);
}

TEST(FindBridgeTest, simple_test_3) {
    std::vector<geometry::vec2> points;
    // square
    points.emplace_back(0, 0);
    points.emplace_back(1, 0);
    points.emplace_back(1, 1);
    points.emplace_back(0, 1);
    // triangle
    points.emplace_back(2, -4);
    points.emplace_back(4, -4);
    points.emplace_back(3, -3);

    std::vector<int> i_left {0, 1, 2, 3};
    std::vector<int> i_right {4, 5, 6};

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    ASSERT_EQ(bridges.size(), 2);
    ASSERT_EQ(bridges[0].first, 0);
    ASSERT_EQ(bridges[0].second, 4);
    ASSERT_EQ(bridges[1].first, 2);
    ASSERT_EQ(bridges[1].second, 5);
}

TEST(FindBridgeTest, middle_test_1) {
    std::vector<geometry::vec2> points;
    // left
    points.emplace_back(2, 0);
    points.emplace_back(5, 1);
    points.emplace_back(5, 3);
    points.emplace_back(2, 5);
    points.emplace_back(0, 3);
    points.emplace_back(1, 1);
    // right
    points.emplace_back(7, 5);
    points.emplace_back(11, 6);
    points.emplace_back(12, 9);
    points.emplace_back(9, 9);
    points.emplace_back(7, 7);

    std::vector<int> i_left {0, 1, 2, 3, 4, 5};
    std::vector<int> i_right {6, 7, 8, 9, 10};

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    ASSERT_EQ(bridges.size(), 2);
    ASSERT_EQ(bridges[0].first, 1);
    ASSERT_EQ(bridges[0].second, 7);
    ASSERT_EQ(bridges[1].first, 3);
    ASSERT_EQ(bridges[1].second, 9);
}

TEST(FindBridgeTest, buggy_test_1) {
    std::vector<geometry::vec2> points;
    // left
    points.emplace_back(0, 0);
    points.emplace_back(1, 0);
    // right
    points.emplace_back(2, 0);
    points.emplace_back(3, 0);

    std::vector<int> i_left {0, 1};
    std::vector<int> i_right {2, 3};

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    ASSERT_EQ(bridges.size(), 1);
    ASSERT_EQ(bridges[0].first, 1);
    ASSERT_EQ(bridges[0].second, 2);
}

TEST(FindBridgeTest, buggy_test_2) {
    std::vector<geometry::vec2> points;
    // left
    points.emplace_back(0, 0);
    points.emplace_back(1, 1);
    // right
    points.emplace_back(2, 2);
    points.emplace_back(3, 3);

    std::vector<int> i_left {0, 1};
    std::vector<int> i_right {2, 3};

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    ASSERT_EQ(bridges.size(), 1);
    ASSERT_EQ(bridges[0].first, 1);
    ASSERT_EQ(bridges[0].second, 2);
}

TEST(FindBridgeTest, random_test_medium) {
    std::uniform_real_distribution<double> uniform(-100, 100);
    std::default_random_engine re;

    std::vector<geometry::vec2> left;
    for (int i = 0; i < 100; i++) {
        left.emplace_back(uniform(re), uniform(re));
    }
    left = geometry::find_convex_hull(left);

    std::vector<geometry::vec2> right;
    for (int i = 0; i < 100; i++) {
        right.emplace_back(300 + uniform(re), uniform(re));
    }
    right = geometry::find_convex_hull(right);

    std::vector<geometry::vec2> points;
    points.insert(points.end(), left.begin(), left.end());
    points.insert(points.end(), right.begin(), right.end());
    std::vector<int> i_left, i_right;
    for (int i = 0; i < (int) left.size(); i++) i_left.push_back(i);
    for (int i = 0; i < (int) right.size(); i++) i_right.push_back((int) left.size() + i);

    auto is_lower = [&points] (int l, int r, const geometry::vec2 &p) {
        return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) < 0;
    };

    auto is_upper = [&points] (int l, int r, const geometry::vec2 &p) {
        return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) > 0;
    };

    std::pair<int, int> lower_bridge;
    std::pair<int, int> upper_bridge;
    for (int l : i_left) {
        for (int r : i_right) {
            bool has_lower = false;
            bool has_upper = false;
            for (const geometry::vec2 &p : points) {
                if (is_lower(l, r, p)) {
                    has_lower = true;
                }
                if (is_upper(l, r, p)) {
                    has_upper = true;
                }
            }
            if (!has_lower) {
                lower_bridge = {l, r};
            }
            if (!has_upper) {
                upper_bridge = {l, r};
            }
        }
    }

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    if (bridges.size() == 2) {
        ASSERT_EQ(lower_bridge.first, bridges[0].first);
        ASSERT_EQ(lower_bridge.second, bridges[0].second);
        ASSERT_EQ(upper_bridge.first, bridges[1].first);
        ASSERT_EQ(upper_bridge.second, bridges[1].second);
    } else {
        ASSERT_EQ(lower_bridge.first, bridges[0].first);
        ASSERT_EQ(lower_bridge.second, bridges[0].second);
    }
}

TEST(FindBridgeTest, random_test_small) {
    std::uniform_int_distribution<int> uniform(-10, 10);
    std::default_random_engine re;

    std::vector<geometry::vec2> left;
    for (int i = 0; i < 10; i++) {
        left.emplace_back(uniform(re), uniform(re));
    }
    left = geometry::find_convex_hull(left);

    std::vector<geometry::vec2> right;
    for (int i = 0; i < 10; i++) {
        right.emplace_back(300 + uniform(re), uniform(re));
    }
    right = geometry::find_convex_hull(right);

    std::vector<geometry::vec2> points;
    points.insert(points.end(), left.begin(), left.end());
    points.insert(points.end(), right.begin(), right.end());
    std::vector<int> i_left, i_right;
    for (int i = 0; i < (int) left.size(); i++) i_left.push_back(i);
    for (int i = 0; i < (int) right.size(); i++) i_right.push_back((int) left.size() + i);

    auto is_lower = [&points] (int l, int r, const geometry::vec2 &p) {
        return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) < 0;
    };

    auto is_upper = [&points] (int l, int r, const geometry::vec2 &p) {
        return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) > 0;
    };

    std::pair<int, int> lower_bridge;
    std::pair<int, int> upper_bridge;
    for (int l : i_left) {
        for (int r : i_right) {
            assert(l != r);
            bool has_lower = false;
            bool has_upper = false;
            for (const geometry::vec2 &p : points) {
                if (is_lower(l, r, p)) {
                    has_lower = true;
                }
                if (is_upper(l, r, p)) {
                    has_upper = true;
                }
            }
            if (!has_lower) {
                lower_bridge = {l, r};
            }
            if (!has_upper) {
                upper_bridge = {l, r};
            }
        }
    }

    auto bridges = geometry::find_bridges(points, i_left, i_right);
    if (bridges.size() == 2) {
        ASSERT_EQ(lower_bridge.first, bridges[0].first);
        ASSERT_EQ(lower_bridge.second, bridges[0].second);
        ASSERT_EQ(upper_bridge.first, bridges[1].first);
        ASSERT_EQ(upper_bridge.second, bridges[1].second);
    } else {
        ASSERT_EQ(lower_bridge.first, bridges[0].first);
        ASSERT_EQ(lower_bridge.second, bridges[0].second);
    }
}
/*
TEST(FindBridgeTest, random_test_alotof_small) {
    std::uniform_real_distribution<double> uniform(-100, 100);
    std::default_random_engine re;

    for (int _test = 0; _test < 100; _test++) {
        std::vector<geometry::vec2> left;
        for (int i = 0; i < 10; i++) {
            left.emplace_back(uniform(re), uniform(re));
        }
        left = geometry::find_convex_hull(left);

        std::vector<geometry::vec2> right;
        for (int i = 0; i < 10; i++) {
            right.emplace_back(300 + uniform(re), uniform(re));
        }
        right = geometry::find_convex_hull(right);

        std::vector<geometry::vec2> points;
        points.insert(points.end(), left.begin(), left.end());
        points.insert(points.end(), right.begin(), right.end());
        std::vector<int> i_left, i_right;
        for (int i = 0; i < (int) left.size(); i++) i_left.push_back(i);
        for (int i = 0; i < (int) right.size(); i++) i_right.push_back((int) left.size() + i);

        auto is_lower = [&points](int l, int r, const geometry::vec2 &p) {
            return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) < 0;
        };

        auto is_upper = [&points](int l, int r, const geometry::vec2 &p) {
            return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) > 0;
        };

        std::pair<int, int> lower_bridge;
        std::pair<int, int> upper_bridge;
        for (int l : i_left) {
            for (int r : i_right) {
                bool has_lower = false;
                bool has_upper = false;
                for (const geometry::vec2 &p : points) {
                    if (is_lower(l, r, p)) {
                        has_lower = true;
                    }
                    if (is_upper(l, r, p)) {
                        has_upper = true;
                    }
                }
                if (!has_lower) {
                    lower_bridge = {l, r};
                }
                if (!has_upper) {
                    upper_bridge = {l, r};
                }
            }
        }

        auto bridges = geometry::find_bridges(points, i_left, i_right);
        if (bridges.size() == 2) {
            ASSERT_EQ(lower_bridge.first, bridges[0].first);
            ASSERT_EQ(lower_bridge.second, bridges[0].second);
            ASSERT_EQ(upper_bridge.first, bridges[1].first);
            ASSERT_EQ(upper_bridge.second, bridges[1].second);
        } else {
            ASSERT_EQ(lower_bridge.first, bridges[0].first);
            ASSERT_EQ(lower_bridge.second, bridges[0].second);
        }
    }
}
 KAKTYS?
 */

TEST(FindBridgeTest, random_test_alotof_small_int) {
    std::uniform_int_distribution<int> uniform(-100, 100);
    std::default_random_engine re;

    for (int _test = 0; _test < 1; _test++) {
        std::vector<geometry::vec2> left;
        for (int i = 0; i < 10; i++) {
            left.emplace_back(uniform(re), uniform(re));
        }
        left = geometry::find_convex_hull(left);

        std::vector<geometry::vec2> right;
        for (int i = 0; i < 10; i++) {
            right.emplace_back(300 + uniform(re), uniform(re));
        }
        right = geometry::find_convex_hull(right);

        std::vector<geometry::vec2> points;
        points.insert(points.end(), left.begin(), left.end());
        points.insert(points.end(), right.begin(), right.end());
        std::vector<int> i_left, i_right;
        for (int i = 0; i < (int) left.size(); i++) i_left.push_back(i);
        for (int i = 0; i < (int) right.size(); i++) i_right.push_back((int) left.size() + i);

        auto is_lower = [&points](int l, int r, const geometry::vec2 &p) {
            return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) < 0;
        };

        auto is_upper = [&points](int l, int r, const geometry::vec2 &p) {
            return geometry::detail::subs_in_line(geometry::line<geometry::vec2>(points[l], points[r]), p) > 0;
        };

        std::pair<int, int> lower_bridge;
        std::pair<int, int> upper_bridge;
        for (int l : i_left) {
            for (int r : i_right) {
                bool has_lower = false;
                bool has_upper = false;
                for (const geometry::vec2 &p : points) {
                    if (is_lower(l, r, p)) {
                        has_lower = true;
                    }
                    if (is_upper(l, r, p)) {
                        has_upper = true;
                    }
                }
                if (!has_lower) {
                    lower_bridge = {l, r};
                }
                if (!has_upper) {
                    upper_bridge = {l, r};
                }
            }
        }

        auto bridges = geometry::find_bridges(points, i_left, i_right);
        if (bridges.size() == 2) {
            ASSERT_EQ(lower_bridge.first, bridges[0].first);
            ASSERT_EQ(lower_bridge.second, bridges[0].second);
            ASSERT_EQ(upper_bridge.first, bridges[1].first);
            ASSERT_EQ(upper_bridge.second, bridges[1].second);
        } else {
            ASSERT_EQ(lower_bridge.first, bridges[0].first);
            ASSERT_EQ(lower_bridge.second, bridges[0].second);
        }
    }
}