//
// Created by gdhsnlvr on 22.01.17.
//

#include <gtest/gtest.h>
#include <algorithm>

std::vector<int> partition(int *begin, int *end, bool flag = false) {
    int length = (int) (end - begin);
    assert(length > 0);

    if (length == 1 || length == 2 || *begin == *(end - 1)) {
        return std::vector<int>(begin, end);
    }

    int *middle = begin + length / 2;

    if (flag) {
        while (middle != end && *middle == *(middle - 1)) middle++;
    } else {
        while (middle != begin && *middle == *(middle - 1)) middle--;
    }

    if (flag && middle == end) return partition(begin, end, !flag);
    if (!flag && middle == begin) return partition(begin, end, !flag);

    auto left = partition(begin, middle, !flag);
    auto right = partition(middle, end, !flag);
    for (int i : left) {
        for (int j : right) {
            assert(i != j);
        }
    }

    std::vector<int> result;
    result.insert(result.end(), left.begin(), left.end());
    result.insert(result.end(), right.begin(), right.end());
    return result;
}

TEST(PartitionTest, test_1) {
    std::vector<int> v {1, 1, 1, 1, 2, 2};
    ASSERT_EQ(v, partition(v.data(), v.data() + v.size()));
}

TEST(PartitionTest, test_2) {
    std::vector<int> v {1, 1, 1, 2, 2, 2};
    ASSERT_EQ(v, partition(v.data(), v.data() + v.size()));
}

TEST(PartitionTest, test_3) {
    std::vector<int> v {1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    ASSERT_EQ(v, partition(v.data(), v.data() + v.size()));
}

TEST(PartitionTest, random_1) {
    std::srand(std::time(nullptr));
    std::vector<int> v;
    int n = std::rand() % 100 + 1;
    for (int i = 0; i < n; i++) {
        v.emplace_back(std::rand() % 5 + 1);
    }
    std::sort(v.begin(), v.end());

    ASSERT_EQ(v, partition(v.data(), v.data() + v.size()));
}

TEST(PartitionTest, random_2) {
    std::srand(std::time(nullptr));
    std::vector<int> v;
    int n = std::rand() % 1000 + 1;
    for (int i = 0; i < n; i++) {
        v.emplace_back(std::rand() % 5 + 1);
    }
    std::sort(v.begin(), v.end());

    ASSERT_EQ(v, partition(v.data(), v.data() + v.size()));
}