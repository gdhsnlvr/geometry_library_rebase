//
// Created by gdhsnlvr on 03.01.17.
//

#include <gtest/gtest.h>

#include "../source/geometry/line.h"
#include "../source/geometry/detail/geometry_detail.h"

TEST(LineVec2Intersection, SimpleIntersection) {
    geometry::line<geometry::vec2> l1(geometry::vec2(0, 0), geometry::vec2(1, 1));
    geometry::line<geometry::vec2> l2(geometry::vec2(2, 0), geometry::vec2(0, 2));
    geometry::vec2 result;

    ASSERT_EQ(geometry::detail::intersect(l1, l2, result), true);
    ASSERT_DOUBLE_EQ(result.x, 1);
    ASSERT_DOUBLE_EQ(result.y, 1);
}

TEST(LineVec2Intersection, DoNotIntersects) {
    geometry::line<geometry::vec2> l1(geometry::vec2(0, 0), geometry::vec2(1, 1));
    geometry::line<geometry::vec2> l2(geometry::vec2(2, 0), geometry::vec2(4, 2));
    geometry::vec2 result;

    ASSERT_EQ(geometry::detail::intersect(l1, l2, result), false);
}

TEST(LineVec2Intersection, BadIntersection) {
    geometry::line<geometry::vec2> l1(geometry::vec2(0, 0), geometry::vec2(1, 0));
    geometry::line<geometry::vec2> l2(geometry::vec2(0, 2), geometry::vec2(1000000, 1));
    geometry::vec2 result;

    ASSERT_EQ(geometry::detail::intersect(l1, l2, result), true);
    ASSERT_DOUBLE_EQ(result.x, 2000000);
    ASSERT_DOUBLE_EQ(result.y, 0);
}