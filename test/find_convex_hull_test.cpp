//
// Created by gdhsnlvr on 22.01.17.
//


#include <gtest/gtest.h>
#include "../source/geometry/vec2.h"
#include "../source/geometry/detail/find_convex_hull_test.h"

TEST(FindConvexHull, simpleTest) {
    std::vector<geometry::vec2> points;
    points.emplace_back(0, 0);
    points.emplace_back(1, 0);
    points.emplace_back(1, 1);
    points.emplace_back(0, 1);
    points.emplace_back(0.5, 0.5);
    points.emplace_back(0.5, 0.4);
    points.emplace_back(0.1, 0.4);
    points.emplace_back(0.6, 0.8);

    points = geometry::find_convex_hull(points);

    ASSERT_EQ(points.size(), 4);

    ASSERT_DOUBLE_EQ(points[0].x, 0);
    ASSERT_DOUBLE_EQ(points[0].y, 0);

    ASSERT_DOUBLE_EQ(points[1].x, 1);
    ASSERT_DOUBLE_EQ(points[1].y, 0);

    ASSERT_DOUBLE_EQ(points[2].x, 1);
    ASSERT_DOUBLE_EQ(points[2].y, 1);

    ASSERT_DOUBLE_EQ(points[3].x, 0);
    ASSERT_DOUBLE_EQ(points[3].y, 1);
}