//
// Created by gdhsnlvr on 11.03.17.
//


#include <gtest/gtest.h>
#include "../source/geometry/polygon.h"
#include "../source/geometry/detail/polygon_detail.h"

TEST(PolygonDifference, simple_test_1) {
    geometry::polygon clip(
            {{0.4, -1}, {0.6, -1}, {0.6, 1}, {0.4, 1}}
    );
    geometry::polygon polygon(
            {{0, 0}, {0, 1}, {1, 1}, {1, 0}}
    );

    auto results = geometry::difference(polygon, clip);
    ASSERT_EQ(results.size(), 2);
}